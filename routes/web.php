<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Przekierowania do MainController
 */
Route::get('/', 'MainController@home');
Route::get('contact', 'MainController@contact');
Route::get('forCompany', 'MainController@forCompany')->name('forCompany');
Route::post('/check', 'MainController@check')->name('check');
Route::get('/check', 'MainController@check')->name('check');
Route::get('error', 'MainController@error');
Route::post('sendQuestionToOrder', 'MainController@sendQuestionToOrder');
Route::post('sendMailToAuthor', 'MainController@sendMailToAuthor');


/**
 * Przekierowania do StaffController
 */
Route::post('updateStatus', 'StaffController@updateStatus')->middleware('CheckLogin');
Route::get('editOrder/{id}', 'StaffController@editOrder')->middleware('CheckLogin')->name('editOrder');
Route::post('newPost', 'StaffController@newPost')->middleware('CheckLogin');
Route::get('panel', 'StaffController@panel')->middleware('CheckLogin');
Route::get('editPost/{id}', 'StaffController@editPost')->middleware('CheckLogin')->name('editPost');
Route::get('deletePost/{id}', 'StaffController@deletePost')->middleware('CheckLogin')->name('deletePost');
Route::post('saveEditedPost', 'StaffController@saveEditedPost')->middleware('CheckLogin');
Route::post('saveDeletedPost', 'StaffController@saveDeletedPost')->middleware('CheckLogin');
Route::post('addItemToBill', 'StaffController@addItemToBill')->middleware('CheckLogin');
Route::get('editBill/{id}', 'StaffController@editBill')->middleware('CheckLogin');
Route::get('deleteBill/{id}', 'StaffController@deleteBill')->middleware('CheckLogin');
Route::post('saveEditedBill', 'StaffController@saveEditedBill')->middleware('CheckLogin');
Route::post('saveDeletedBill', 'StaffController@saveDeletedBill')->middleware('CheckLogin');
Route::get('addOrder', 'StaffController@addOrder')->middleware('CheckLogin');
Route::get('account', 'StaffController@account')->middleware('CheckLogin');
Route::get('editCustomer/{id}', 'StaffController@editCustomer')->middleware('CheckLogin')->name('editCustomer');
Route::post('addNewOrder', 'StaffController@addNewOrder')->middleware('CheckLogin')->name('addNewOrder');
Route::get('editOrderName/{id}', 'StaffController@editOrderName')->middleware('CheckLogin');
Route::post('saveEditedOrderName', 'StaffController@saveEditedOrderName')->middleware('CheckLogin');
Route::post('saveEditedCustomer', 'StaffController@saveEditedCustomer')->middleware('CheckLogin');
Route::get('editAccount', 'StaffController@editAccount')->middleware('CheckLogin');
Route::post('saveEditedAccount', 'StaffController@saveEditedAccount')->middleware('CheckLogin');
Route::get('editPassword', 'StaffController@editPassword')->middleware('CheckLogin');
Route::post('saveEditedPassword', 'StaffController@saveEditedPassword')->middleware('CheckLogin');
Route::get('deleteOrder/{id}', 'StaffController@deleteOrder')->middleware('CheckLogin');
Route::post('confirmDeletedOrder', 'StaffController@confirmDeletedOrder')->middleware('CheckLogin')->name('confirmDeletedOrder');


/**
 * Przekierowania do BossController
 */
Route::get('company', 'BossController@company')->middleware('CheckBossLogin');
Route::get('editCompany', 'BossController@editCompany')->middleware('CheckBossLogin');
Route::get('employeeOrders/{id}', 'BossController@employeeOrders')->middleware('CheckBossLogin')->name('employeeOrders');
Route::get('employeeAccount/{id}', 'BossController@employeeAccount')->middleware('CheckBossLogin')->name('employeeAccount');
Route::get('editEmployeeAccount/{id}', 'BossController@editEmployeeAccount')->middleware('CheckBossLogin')->name('editEmployeeAccount');
Route::get('editEmployeePassword/{id}', 'BossController@editEmployeePassword')->middleware('CheckBossLogin')->name('editEmployeePassword');
Route::post('saveEditedEmployeePassword', 'BossController@saveEditedEmployeePassword')->middleware('CheckBossLogin');
Route::post('saveEditedEmployeeAccount', 'BossController@saveEditedEmployeeAccount')->middleware('CheckBossLogin');
Route::get('archiveOrder/{id}', 'BossController@archiveOrder')->middleware('CheckBossLogin');
Route::get('saveOrderToArchive', 'BossController@saveOrderToArchive')->middleware('CheckBossLogin');
Route::get('unarchiveOrder/{id}', 'BossController@unarchiveOrder')->middleware('CheckBossLogin');
Route::get('activeOrder', 'BossController@activeOrder')->middleware('CheckBossLogin');
Route::post('saveEditedCompany', 'BossController@saveEditedCompany')->middleware('CheckBossLogin');
Route::get('archive', 'BossController@archive')->middleware('CheckBossLogin');
Route::get('active', 'BossController@active')->middleware('CheckBossLogin');
Route::get('archiveDetails/{id}', 'BossController@archiveDetails')->middleware('CheckBossLogin')->name('archiveDetails');
Route::get('addEmployee', 'BossController@addEmployee')->middleware('CheckBossLogin');
Route::post('addNewEmployee', 'BossController@addNewEmployee')->middleware('CheckBossLogin');
Route::get('customers', 'BossController@customers')->middleware('CheckBossLogin');
Route::get('customerOrders/{id}', 'BossController@customerOrders')->middleware('CheckBossLogin');
Route::get('deleteCustomer/{id}', 'BossController@deleteCustomer')->middleware('CheckBossLogin');
Route::post('confirmDeletedCustomer', 'BossController@confirmDeletedCustomer')->middleware('CheckBossLogin');
Route::get('employees/{id}', 'BossController@employees')->middleware('CheckBossLogin');
Route::get('deleteEmployee/{id}', 'BossController@deleteEmployee')->middleware('CheckBossLogin');
Route::post('confirmDeletedEmployee', 'BossController@confirmDeletedEmployee')->middleware('CheckBossLogin');
Route::get('editEmployeeOrder/{id}', 'BossController@editEmployeeOrder')->middleware('CheckBossLogin')->name('editEmployeeOrder');
Route::post('saveEditedEmployeeOrder', 'BossController@saveEditedEmployeeOrder')->middleware('CheckBossLogin');
Route::get('doBoss/{id}', 'BossController@doBoss')->middleware('CheckBossLogin');
Route::post('confirmDoBoss', 'BossController@cofirmDoBoss')->middleware('CheckBossLogin');
Route::get('doEmployee/{id}', 'BossController@doEmployee')->middleware('CheckBossLogin');
Route::post('confirmDoEmployee', 'BossController@cofirmDoEmployee')->middleware('CheckBossLogin');

/**
 * Przekierowania do AdminController
 */
Route::get('company/{id}', 'AdminController@company')->middleware('CheckAdminLogin')->name('company');
Route::get('editCompany/{id}', 'AdminController@editCompany')->middleware('CheckAdminLogin')->name('editCompany');
Route::get('editAccountt/{id}', 'AdminController@editAccount')->middleware('CheckAdminLogin')->name('editAccount');
Route::post('saveEditedAccountt', 'AdminController@saveEditedAccount')->middleware('CheckAdminLogin')->name('saveEditedAccount');
Route::get('companyOrders/{id}', 'AdminController@companyOrders')->middleware('CheckAdminLogin')->name('companyOrders');
Route::get('companyArchiveOrders/{id}', 'AdminController@companyArchiveOrders')->middleware('CheckAdminLogin')->name('companyArchiveOrders');
Route::get('companyCustomers/{id}', 'AdminController@companyCustomers')->middleware('CheckAdminLogin')->name('companyCustomers');
Route::get('companies', 'AdminController@companies')->middleware('CheckAdminLogin');
Route::get('deleteCompany/{id}', 'AdminController@deleteCompany')->middleware('CheckAdminLogin');
Route::post('confirmDeletedCompany', 'AdminController@confirmDeletedCompany')->middleware('CheckAdminLogin');
Route::get('addCompany', 'AdminController@addCompany')->middleware('CheckAdminLogin');
Route::post('createCompany', 'AdminController@createCompany')->middleware('CheckAdminLogin')->name('createCompany');
Route::get('addEmployee/{id}', 'AdminController@addEmployee')->middleware('CheckAdminLogin');
Route::post('addEmployeeToCompany', 'AdminController@addEmployeeToCompany')->middleware('CheckAdminLogin');
Route::post('addEmployeeToCompany', 'AdminController@addEmployeeToCompany')->middleware('CheckAdminLogin');
Route::get('allCustomers', 'AdminController@allCustomers')->middleware('CheckAdminLogin');
Route::get('allUsers', 'AdminController@allUsers')->middleware('CheckAdminLogin');
Route::get('allOrders', 'AdminController@allOrders')->middleware('CheckAdminLogin');
Route::get('allArchiveOrders', 'AdminController@allArchiveOrders')->middleware('CheckAdminLogin');
Route::get('addOrderToCompany/{id}', 'AdminController@addOrderToCompany')->middleware('CheckAdminLogin');
Route::post('addNewOrderToCompany', 'AdminController@addNewOrderToCompany')->middleware('CheckAdminLogin');



/**
 * Przekierowania do rejestracji i logowania
 */
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/sendMail', 'MainController@sendMail');

