<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Adam',
                'lastname' => 'Kowalski',
                'phone' => '123456789',
                'password' => bcrypt('admin'),
                'status' => 'admin',
                'email' => 'admin@yopmail.com',
                'company_id' => null,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Paweł',
                'lastname' => 'Kowalski',
                'phone' => '236795264',
                'password' => bcrypt('boss'),
                'status' => 'boss',
                'email' => 'pkowalski@yopmail.com',
                'company_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Weronika',
                'lastname' => 'Nowak',
                'phone' => '872520592',
                'password' => bcrypt('user'),
                'status' => 'employee',
                'email' => 'wnowak@yopmail.com',
                'company_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Maciej',
                'lastname' => 'Kacprzak',
                'phone' => '576302968',
                'password' => bcrypt('user'),
                'status' => 'employee',
                'email' => 'mkacprzak@yopmail.com',
                'company_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Hanna',
                'lastname' => 'Zielińska',
                'phone' => '324683219',
                'password' => bcrypt('boss'),
                'status' => 'boss',
                'email' => 'hzielinska@yopmail.com',
                'company_id' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Jakub',
                'lastname' => 'Wilk',
                'phone' => '958302917',
                'password' => bcrypt('user'),
                'status' => 'employee',
                'email' => 'jwilk@yopmail.com',
                'company_id' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Maria',
                'lastname' => 'Kozioł',
                'phone' => '490498175',
                'password' => bcrypt('user'),
                'status' => 'employee',
                'email' => 'mkoziol@yopmail.com',
                'company_id' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]

        ]);
    }
}
