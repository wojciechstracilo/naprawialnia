<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            [
                'name' => 'Repair Company',
                'address' => 'Rzeszów ul. Długa 44',
                'description' => 'Firma zajmuje się naprawą elektroniki. Działa na rynku od 4 lat',
                'phone' => '123456789',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'name' => 'Smart-Serwis',
                'address' => 'Kolbuszowa ul. Kwiatowa 10/5',
                'description' => 'Serwis naprawia zepsute telefony i smartfony',
                'phone' => '546289183',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
