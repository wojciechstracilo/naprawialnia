<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompaniesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CustomerTableSeeder::class);
        $this->call(StatusesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(BillsTableSeeder::class);
    }
}
