<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => 'Michał',
                'lastname' => 'Białek',
                'address' => 'Jasło 152',
                'email' => 'mbialek@yopmail.com',
                'phone' => '625418952',
                'company_id' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Natalia',
                'lastname' => 'Kot',
                'address' => 'Kolbuszowa ul. Kwiatowa 21/5',
                'email' => 'nkot@yopmail.com',
                'phone' => '638456129',
                'company_id' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Weronika',
                'lastname' => 'Nowak',
                'address' => 'Stalowa Wola ul. Rzeszowska 213',
                'email' => 'wnowak@yopmail.com',
                'phone' => '638456129',
                'company_id' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],

            [
                'name' => 'Mateusz',
                'lastname' => 'Wiśniewski',
                'address' => 'Jarosław ul. Wiosenna 8A/15',
                'email' => 'mwisniewski@yopmail.com',
                'phone' => '638456129',
                'company_id' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
        ]);
    }
}
