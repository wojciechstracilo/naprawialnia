<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'token' => str_random(10),
                'name' => 'Laptop V3-771G',
                'status_id' => '1',
                'employee_id' => '3',
                'active' => 'true',
                'customer_id' => '1',
                'company_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'token' => str_random(10),
                'name' => 'Router Archer C9',
                'status_id' => '1',
                'employee_id' => '4',
                'active' => 'true',
                'customer_id' => '2',
                'company_id' => '1',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'token' => str_random(10),
                'name' => 'Smartfon S9',
                'status_id' => '1',
                'employee_id' => '6',
                'active' => 'true',
                'customer_id' => '3',
                'company_id' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'token' => str_random(10),
                'name' => 'Telefon N32',
                'status_id' => '1',
                'employee_id' => '7',
                'active' => 'true',
                'customer_id' => '4',
                'company_id' => '2',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],


        ]);
    }
}
