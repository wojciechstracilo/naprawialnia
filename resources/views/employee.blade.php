@extends('master')

@section('title')
    Widok pracownika
@stop

@section('content')

    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Naprawy które prowadzisz:</h3>
            <p class="text-muted">Zakończone naprawy po 7 dniach zostaną automatycznie przeniesione do archiwum firmy</p>
        @foreach($orders as $order)
            <hr>

            <h4><b>Nazwa:</b> {{ $order->name }}</h4>
            <p><b>Imię i nazwisko
                    klienta:</b> {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}</p>
            <p><b>Identyfikator zamówienia:</b> {{ $order->token }}</p>
            <p><b>Status:</b> {{ \App\Status::find($order->status_id)->name }}</p>

            <a class="btn btn-primary" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
            <a class="btn btn-danger" href="/deleteOrder/{{$order->order_id}}">Usuń</a>

        @endforeach
<hr>
    </div>

@stop