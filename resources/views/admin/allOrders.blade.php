@extends('master')

@section('title')
    Wszystkie naprawy
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif


        <h3  style="background-color: #95a5a6; color: white; padding: 10px 0">Lista wszystkich aktywnych napraw w bazie</h3>
        <hr>


        @foreach($orders as $order)


            <h4><b>Nazwa:</b> {{$order->name}}</h4>
            <p><b>Firma:</b> {{\App\Company::find($order->company_id)->name}}</p>
            <p><b>Imię i nazwisko klienta:</b> {{\App\Customer::find($order->customer_id)->name}} {{\App\Customer::find($order->customer_id)->lastname}}</p>
                <p><b>Identyfikator naprawy:</b> {{ $order->token }}</p>
                <p><b>Status:</b> {{\App\Status::find($order->status_id)->name}}</p>
                <p><b>Data dodania:</b> {{$order->created_at}}</p>

            <div class="col-xs-12">
                <a class="btn btn-info" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
                <a class="btn btn-warning" href="/archiveOrder/{{$order->order_id}}">Archiwizuj</a>
                <a class="btn btn-danger" href="/deleteOrder/{{$order->order_id}}">Usuń</a>
            </div>

            <div class="col-xs-12">
                <hr>

            </div>

        @endforeach
            {{ $orders->links() }}

            <div style="width: 100%; display: table"></div>
    </div>
@stop