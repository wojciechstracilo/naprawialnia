@extends('master')

@section('title')
    Szczegóły firmy
@stop

@section('content')
    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Firma: <b><i>{{$company->name}}</i></b></h3>
        <hr>

        <h4>Panel skrótów</h4>
        <div class="col-xs-12">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="btn-group btn-group-justified">
                    <a href="/companyOrders/{{$company->company_id}}" class="btn btn-info">Aktywne naprawy</a>
                    <a href="/addOrderToCompany/{{$company->company_id}}" class="btn btn-info">Dodaj naprawę</a>
                    <a href="/addEmployee/{{$company->company_id}}" class="btn btn-info">Dodaj pracownika</a>
                    <a href="/companyCustomers/{{$company->company_id}}" class="btn btn-info">Pokaż klientów</a>
                </div>
            </div>
        </div>
        <div style="width: 100%; display: table; margin-top: 60px">

            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dane firmy</h3>

            <div class="list-group">
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Nazwa firmy</h4>
                    <p class="list-group-item-text">{{$company->name}}</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Adres</h4>
                    <p class="list-group-item-text">{{$company->address}}</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Telefon</h4>
                    <p class="list-group-item-text">{{$company->phone}}</p>
                </a>
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Opis firmy</h4>
                    <p class="list-group-item-text">{{$company->description}}</p>
                </a>

                <a class="btn btn-info" href="/editCompany/{{$company->company_id}}" style="margin-top: 10px">Edytuj
                    dane
                    firmy</a>

            </div>


            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Klienci przypisani do firmy</h3>
            <h4>Liczba klientów przypisanych do firmy: {{ $customersCount }}</h4>
            <a class="btn btn-primary" href="/companyCustomers/{{$company->company_id}}" style="margin-top: 10px">Zobacz
                wszystkich klientów</a>

            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Naprawy w firmie</h3>
            <h4>Liczba aktywnych napraw: {{$ordersActive}}</h4>
            <h4>Liczba archiwalnych napraw: {{$ordersArchive}}</h4>
            <a class="btn btn-primary" href="/companyOrders/{{$company->company_id}}" style="margin-top: 10px">Zobacz aktywne
                naprawy</a>
            <a class="btn btn-info" href="/companyArchiveOrders/{{$company->company_id}}"
               style="margin-top: 10px; margin-left: 3px">Zobacz archiwum
                napraw</a>


            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Szefem firmy jest/są:</h3>
            <hr>
            @foreach($bosses as $boss)
                <h4><b>Nazwisko i imię: </b>{{$boss->lastname}} {{$boss->name}}</h4>
                <p><b>Telefon:</b> {{$boss->phone}}</p>
                <p><b>Mail:</b> {{$boss->email}}</p>
                <a class="btn btn-info" href="/editAccountt/{{$boss->user_id}}" style="margin-top: 10px">Edytuj
                    profil</a>
                    @if(\App\User::where('status', 'boss')->where('company_id', $boss->company_id)->count() > 1)
                        <a class="btn btn-danger" href="/deleteEmployee/{{$boss->user_id}}" style="margin-top: 10px;margin-left: 3px">Usuń szefa</a>
                    @else
                        <a class="btn btn-danger disabled" href="" style="margin-top: 10px;margin-left: 3px">Nie można usunąć jedynego szefa</a>
                    @endif
                <a class="btn btn-warning" style="margin-top: 10px;margin-left: 3px" href="/doEmployee/{{$boss->user_id}}" style="margin-top: 10px">Zmień stanowisko</a>

                <hr>
            @endforeach


            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista pracowników firmy:</h3>
            <hr>
            @foreach($employees as $employee)
                <h4><b>Nazwisko i imię: </b> {{ $employee->lastname }} {{ $employee->name }}</h4>
                <p><b>Telefon:</b> {{$employee->phone}}</p>
                <p><b>Mail:</b> {{$employee->email}}</p>
                <a class="btn btn-info" href="/editAccountt/{{$employee->user_id}}" style="margin-top: 10px">Edytuj
                    profil</a>
                <a class="btn btn-danger" href="/deleteEmployee/{{$employee->user_id}}" style="margin-top: 10px;margin-left: 3px">Usuń użytkownika</a>
                <a class="btn btn-warning" style="margin-top: 10px;margin-left: 3px" href="/doBoss/{{$employee->user_id}}" style="margin-top: 10px">Zmień stanowisko</a>

                <hr>
            @endforeach
            {{ $employees->links() }}



        </div>
    </div>
@endsection
