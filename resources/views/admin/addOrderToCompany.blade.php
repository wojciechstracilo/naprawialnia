@extends('master')

@section('title')
    Dodaj naprawę
@stop


<script type="text/javascript">
    function check() {
        if (document.getElementById('new').checked == true) {
            document.getElementById('oldCustomer').disabled = true;

            document.getElementById('customerName').disabled = false;
            document.getElementById('customerLastName').disabled = false;
            document.getElementById('phone').disabled = false;
            document.getElementById('email').disabled = false;
            document.getElementById('address').disabled = false;

        } else if (document.getElementById('old').checked == true) {
            document.getElementById('oldCustomer').disabled = false;

            document.getElementById('customerName').disabled = true;
            document.getElementById('customerLastName').disabled = true;
            document.getElementById('phone').disabled = true;
            document.getElementById('email').disabled = true;
            document.getElementById('address').disabled = true;
        }
    }
</script>

@section('content')
    <div class="jumbotron">
        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dodaj naprawę w firmie <b>{{$company->name}}</b></h3>
        <h4>Możesz dodać nową naprawę, wprowadzając dane nowego klienta, lub wybrać już istniejącego klienta.</h4>
            <h4>Dodatkowo z listy wybierz pracownika, który będzie nadzorował naprawę.</h4>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #2c3e50; color: white">Dodaj naprawę</div>
                        <div class="panel-body">
                            {{Form::open(['url' => 'addNewOrderToCompany', 'method' => 'post', 'class' => 'form-horizontal'])}}
                            {{ Form::hidden('company_id', Auth::user()->company_id) }}
                            {{ Form::hidden('employee_id', Auth::user()->user_id) }}

                                <div class="form-group{{ $errors->has('employee') ? ' has-error' : '' }}">
                                    <label for="employee" class="col-md-4 control-label">Pracownik</label>

                                    <div class="col-md-6">
                                        <select  style="text-align-last: center" class="form-control" name="employee">
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->user_id}}">{{ $employee->name }} {{ $employee->lastname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nazwa naprawy</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="50" id="name" type="text" class="form-control"
                                           name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" style="margin-top: 13px;">Klient</label>
                                <div class="col-md-6">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radio" value="new" id="new" onclick="check()"
                                                   checked="">
                                            Nowy klient
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="radio" value="old" id="old" onclick="check()">
                                            Klient już obecny w systemie
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('oldCustomer') ? ' has-error' : '' }}">
                                <label for="oldCustomer" class="col-md-4 control-label">Klient</label>

                                <div class="col-md-6">
                                    <select style="text-align-last: center" class="form-control" name="oldCustomer" id="oldCustomer" disabled>
                                        @foreach($customers as $customer)
                                            <option value="{{$customer->customer_id}}">{{ $customer->lastname }} {{ $customer->name }}
                                                || {{ $customer->address }} || {{ $customer->email }}
                                                || {{ $customer->phone }}</option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="form-group{{ $errors->has('customerName') ? ' has-error' : '' }}">
                                <label for="customerName" class="col-md-4 control-label">Imię klienta</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="30" id="customerName" type="text" class="form-control"
                                           name="customerName" value="{{ old('customerName') }}" required autofocus>

                                    @if ($errors->has('customerName'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('customerName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('customerLastName') ? ' has-error' : '' }}">
                                <label for="customerLastName" class="col-md-4 control-label">Nazwisko klienta</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="30" id="customerLastName" type="text"
                                           class="form-control" name="customerLastName"
                                           value="{{ old('customerLastName') }}" required autofocus>

                                    @if ($errors->has('customerLastName'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('customerLastName') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Telefon klienta</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" id="phone" min="1" max="999999999" type="number" class="form-control"
                                           name="phone" value="{{ old('phone') }}" required autofocus>

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Adres e-mail klienta</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="50" id="email" type="email" class="form-control"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label for="address" class="col-md-4 control-label">Adres zamieszkania klienta</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="100" id="address" type="text" class="form-control"
                                           name="address" value="{{ old('address') }}" required>

                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Dodaj naprawę
                                    </button>
                                </div>
                            </div>
                            {{Form::hidden('company_id', $company->company_id)}}
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
