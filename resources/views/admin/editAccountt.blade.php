@extends('master')

@section('title')
    Edycja dancyh klienta
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-danger">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        {{ Form::open(['url' => '/saveEditedAccountt', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}


        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Edytuj dane użytkownika z firmy
                <b><i>{{\App\Company::find($user->company_id)->name}}</i></b></h3>
            <hr>

            <label for="textArea" class="control-label col-sm-12">Imię</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('name', $user->name, array('class' => 'form-control', 'required', 'style' => 'text-align: center')) }}
            </div>

            <label for="textArea" class="control-label col-sm-12">Nazwisko</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('lastname', $user->lastname, array('class' => 'form-control', 'required', 'style' => 'text-align: center')) }}
            </div>

            <label for="textArea" class="control-label col-sm-12">Mail</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('email', $user->email, array('class' => 'form-control', 'required', 'style' => 'text-align: center')) }}
            </div>
            <label for="textArea" class="control-label col-sm-12">Telefon</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('phone', $user->phone, array('class' => 'form-control', 'required', 'style' => 'text-align: center')) }}
            </div>

            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-xs-12"><h3>Aby zmienić dane, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label col-sm-12 control-label">Hasło</label>

                <div class="form-group col-sm-6  col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label col-sm-12 control-label">Powtórz hasło</label>

                <div class="form-group col-sm-6  col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">

                <div class="col-md-12">

                    {{ Form::hidden('user_id', $user->user_id) }}
                    {{ Form::hidden('back_to', url()->previous()) }}
                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}
                    <a class="btn btn-default" href="{{url()->previous()}}">Anuluj</a>
                </div>
            </div>

        </fieldset>


    </div>

@stop