@extends('master')

@section('title')
    Edycja danych firmy
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        {{ Form::open(['url' => '/saveEditedCompany', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Edytuj dane firmy</h3>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="name" class="control-label col-sm-12 control-label">Nazwa firmy</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="name" type="text" class="form-control"
                           name="name" value="{{ $company->name }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Adres</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('address', $company->address, array('class' => 'form-control', 'required', 'style' => 'text-align: center')) }}
            </div>

            <label for="textArea" class="control-label col-sm-12">Telefon</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::number('phone', $company->phone, array('class' => 'form-control', 'required', 'style' => 'text-align: center')) }}
            </div>

            <label for="textArea" class="control-label col-sm-12">Opis</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::textarea('description', $company->description, array('class' => 'form-control', 'required', 'rows' => 3 ,'style' => 'text-align: center; resize: vertical')) }}
            </div>

            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-xs-12"><h3>Aby zmienić dane firmy, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <label for="textArea" class="control-label col-sm-12">Wpisz hasło</label>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Powtórz hasło</label>
            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">

                <div class="col-md-12">

                    {{ Form::hidden('company_id', $company->company_id) }}
                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}
                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>
                </div>
            </div>

        </fieldset>


    </div>

@stop