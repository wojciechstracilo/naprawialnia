@extends('master')

@section('title')
    Wszyscy użytkownicy
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista wszystkich użytkowników</h3>
            <p class="text-muted">Spis w kolejności alfabetycznej</p>
        <hr>

        @foreach($users as $user)

            <h4><b>Nazwisko i imię:</b> {{$user->lastname}} {{$user->name}}</h4>
            <p><b>Firma:</b> {{ \App\Company::find($user->company_id)->name }}</p>
            <p><b>Mail:</b> {{$user->email}}</p>
            <p><b>Telefon:</b> {{$user->phone}}</p>
            <p><b>Status:</b>
                @if($user->status == 'boss')
                    Szef
                @else
                    Pracownik
                @endif
            </p>
            <div class="col-xs-12">
                <a class="btn btn-primary" href="/editAccountt/{{$user->user_id}}">Edytuj dane</a>

                @if($user->status == 'boss')
                    @if(\App\User::where('status', 'boss')->where('company_id', $user->company_id)->count() > 1)
                        <a class="btn btn-danger" href="/deleteEmployee/{{$user->user_id}}">Usuń użytkownika</a>
                    @else
                        <a class="btn btn-danger disabled" href="">Nie można usunąć jedynego szefa</a>
                    @endif
                @elseif($user->status == 'employee')
                    <a class="btn btn-danger" href="/deleteEmployee/{{$user->user_id}}">Usuń użytkownika</a>
                @endif
            </div>

            <div class="col-xs-12">
                <hr>
            </div>

        @endforeach

        {{ $users->links() }}
        <div style="width: 100%; display: table"></div>

    </div>

@stop