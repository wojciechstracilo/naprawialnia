@extends('master')

@section('title')
    Dodaj pracownika
@stop


@section('content')
    <div class="jumbotron">
        @if(Session::has('message'))
            <div class="alert alert-danger">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dodaj pracownika do firmy
            <b><i>{{$company->name}}</i></b></h3>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #2c3e50; color: white">Dodaj
                            pracownika</div>
                        <div class="panel-body">
                            {{Form::open(['url' => 'addEmployeeToCompany', 'method' => 'post', 'class' => 'form-horizontal'])}}
                            {{ Form::hidden('company_id', $company->company_id) }}


                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Imię</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="30" id="name" type="text" class="form-control"
                                           name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                <label for="lastname"  class="col-md-4 control-label">Nazwisko</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="30" id="lastname" type="text" class="form-control"
                                           name="lastname" value="{{ old('lastname') }}" required autofocus>

                                    @if ($errors->has('lastname'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Telefon</label>

                                <div class="col-md-6">
                                    <input style="text-align: center"  type="number" min="1" max="999999999" id="phone" class="form-control"
                                           name="phone" value="{{ old('phone') }}" required autofocus>

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">Adres e-mail</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" maxlength="50" id="email" type="email" class="form-control"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Hasło</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" minlength="6" id="password" type="password" class="form-control"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                                <label for="password-confirm" class="col-md-4 control-label">Powtórz hasło</label>

                                <div class="col-md-6">
                                    <input style="text-align: center" minlength="6" id="password-confirm" type="password"
                                           class="form-control" name="password_confirmation" required>

                                    @if ($errors->has('password-confirm'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Dodaj pracownika
                                    </button>
                                </div>
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
