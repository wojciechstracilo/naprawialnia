@extends('master')

@section('title')
    Lista firm
@stop

@section('content')

    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista firm zarejestrowanych w
            systemie:</h3>
            <p class="text-muted">Spis w kolejności alfabetycznej</p>

            <table class="table table-striped table-hover"
               style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
            <hr>

            @foreach($companies as $company)

                <h4><b>Nazwa firmy:</b> {{ $company->name }}</h4>
                <p><b>Adres:</b> {{ $company->address }}</p>
                <p><b>Telefon:</b> {{ $company->phone }}</p>
                <p><b>Opis firmy:</b> {{ $company->description }}</p>

                <a class="btn btn-info" href="/company/{{$company->company_id}}">Szczegóły</a>
                <a class="btn btn-danger" href="/deleteCompany/{{$company->company_id}}"
                   style="margin-left: 3px">Usuń</a>

                <hr>
            @endforeach

            {{ $companies->links() }}
        </table>


    </div>


@stop