@extends('master')

@section('title')
    Lista napraw firmy {{$company->name}}
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif


        <div style="width: 100%; display: table">
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Wszystkie aktualne naprawy firmy
                <i><b>{{$company->name}}</b></i>:</h3>
            <hr>
            @foreach($orders as $order)



                <h4><b>Nazwa:</b> {{ $order->name }}</h4>
                <p><b>Ostatnia modyfikacja:</b> {{ \App\Post::where('order_id', $order->order_id)->max('updated_at') }}
                </p>
                <p><b>Status:</b> {{ \App\Status::find($order->status_id)->name }}</p>

                <p><b>Imię i nazwisko
                        klienta:</b> {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}
                </p>


                <p>
                    <a class="btn btn-primary" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
                    <a class="btn btn-warning" href="/archiveOrder/{{$order->order_id}}">Dodaj do archiwum</a>
                    <a class="btn btn-danger" href="/deleteOrder/{{$order->order_id}}">Usuń</a>
                </p>
                <hr>
            @endforeach
            {{ $orders->links() }}

            <a class="btn btn-default" href="/company/{{$company->company_id}}">Powrót</a>

        </div>


    </div>

@stop