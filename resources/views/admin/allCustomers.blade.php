@extends('master')

@section('title')
    Wszyscy klienci
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista wszystkich klientów w bazie</h3>
            <p class="text-muted">Spis w kolejności alfabetycznej</p>

            <hr>

        @foreach($customers as $customer)

            <h4><b>Nazwisko i imię: </b>{{$customer->lastname}} {{$customer->name}}</h4>
            <p><b>Przypisany do firmy:</b> {{ \App\Company::find($customer->company_id)->name }}</p>
            <h5><b>Adres:</b> {{$customer->address}}</h5>
            <p><b>Telefon:</b> {{$customer->phone}}</p>
            <p><b>Mail:</b> {{$customer->email}}</p>

            <div class="col-xs-12">
                <a class="btn btn-info" href="/customerOrders/{{$customer->customer_id}}">Naprawy</a>
                <a class="btn btn-primary" href="/editCustomer/{{$customer->customer_id}}">Edytuj dane</a>
                <a class="btn btn-danger" href="/deleteCustomer/{{$customer->customer_id}}">Usuń klienta</a>
            </div>

            <div class="col-xs-12">
                <hr>
            </div>

        @endforeach
        {{ $customers->links() }}

        <div style="width: 100%; display: table"></div>
    </div>
@stop