@extends('master')

@section('title')
    Usuwanie firmy
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/confirmDeletedCompany', 'method' => 'post', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Na pewno chcesz usunąć firmę?</h3>
            <h4 class="text-danger">UWAGA! Razem z firmą zostaną usunięci wszyscy jej użytkownicy oraz wszystkie
                naprawy. Nie będzie możliwości odzyskania danych.</h4>
            <hr>
            {{ Form::hidden('company_id', $company->company_id) }}


            <h4><b>Nazwa firmy:</b> {{ $company->name }}</h4>
            <p><b>Adres:</b> {{ $company->address }}</p>
            <p><b>Imię i nazwisko
                    szefa:</b> {{ $boss->name }} {{ $boss->lastname }}</p>
            <p><b>Telefon: </b>{{ $company->phone }}</p>
            <p><b>Opis:</b> {{ $company->description }}</p>

            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-xs-12"><h3>Aby usunąć firmę, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label col-sm-12 control-label">Hasło</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label col-sm-12 control-label">Powtórz hasło</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Usuń', ['class' => 'btn btn-danger', 'name' => 'submit'])!!}
                    {{ Form::close() }}

                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>


    </div>
@stop