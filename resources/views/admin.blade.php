@extends('master')

@section('title')
    Widok admina
@stop

@section('content')

    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif

        <div style="width: 100%; display: table">

            <h4 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista ostatnio zarejestrowanych
                napraw:</h4>

            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-striped table-hover"
                       style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                    <tr>
                        <th style="text-align: center">Nazwa</th>
                        <th style="text-align: center">Firma</th>
                        <th style="text-align: center">Data dodania</th>
                        <th style="text-align: center">Więcej</th>
                    </tr>
                    @foreach($orders as $order)

                        <tr>
                            <td> {{ $order->name }} </td>
                            <td> {{ \App\Company::find($order->company_id)->name }} </td>
                            <td> {{ date('d-m-Y', strtotime($order->created_at)) }}</td>
                            <td>
                                <a class="btn btn-primary  btn-xs" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
                                <a class="btn btn-danger btn-xs" href="/deleteOrder/{{$order->order_id}}">Usuń</a>


                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>

        </div>

        <div style="width: 100%; display: table;">
            <h4 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista ostatnio zarejestrowanych
                użytkowników:</h4>
            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-striped table-hover"
                       style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                    <tr>
                        <th style="text-align: center">Imię i nazwisko</th>
                        <th style="text-align: center">Firma</th>
                        <th style="text-align: center">Data rejestracji</th>
                        <th style="text-align: center">Więcej</th>
                    </tr>
                    @foreach($users as $user)

                        <tr>
                            <td> {{ $user->name }}  {{ $user->lastname }} </td>
                            <td> {{ \App\Company::find($user->company_id)->name }} </td>
                            <td> {{ date('d-m-Y', strtotime($user->created_at)) }}</td>
                            <td>
                                <a class="btn btn-primary  btn-xs" href="/editAccountt/{{$user->user_id}}">Dane</a>
                                <a class="btn btn-danger  btn-xs" href="/deleteEmployee/{{$user->user_id}}">Usuń</a>

                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>

        </div>


        <div style="width: 100%; display: table">

            <h4 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista ostatnio dodanych
                klientów:</h4>

            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-striped table-hover"
                       style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                    <tr>
                        <th style="text-align: center">Imię i nazwisko</th>
                        <th style="text-align: center">Firma</th>
                        <th style="text-align: center">Data dodania</th>
                        <th style="text-align: center">Więcej</th>
                    </tr>
                    @foreach($customers as $customer)

                        <tr>
                            <td> {{ $customer->name }} {{ $customer->lastname }} </td>
                            <td> {{ \App\Company::find($customer->company_id)->name }} </td>
                            <td> {{ date('d-m-Y', strtotime($customer->created_at)) }}</td>
                            <td>
                                <a class="btn btn-primary btn-xs" href="/customerOrders/{{$customer->customer_id}}">Naprawy</a>
                                <a class="btn btn-danger btn-xs" href="/deleteCustomer/{{$customer->customer_id}}">Usuń</a>


                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>

        </div>

        <div style="width: 100%; display: table">

            <h4 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista ostatnio zarejestrowanych
                firm:</h4>

            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-striped table-hover"
                       style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                    <tr>
                        <th style="text-align: center">Nazwa</th>
                        <th style="text-align: center">Data rejestracji</th>
                        <th style="text-align: center">Więcej</th>
                    </tr>
                    @foreach($companies as $company)

                        <tr>
                            <td> {{ $company->name }} </td>
                            <td> {{ date('d-m-Y', strtotime($company->created_at)) }}</td>
                            <td>
                                <a class="btn btn-primary btn-xs"
                                   href="/company/{{$company->company_id}}">Szczegóły</a>
                                <a class="btn btn-danger btn-xs" href="/deleteCompany/{{$company->company_id}}" style="margin-left: 3px">Usuń</a>

                            </td>

                        </tr>
                    @endforeach
                </table>
            </div>

        </div>


    </div>



@stop