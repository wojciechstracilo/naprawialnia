<!DOCTYPE html>
@extends('master')

@section('title')
    Kontakt
@stop

<body>


@section('content')

    <div class="row content">

        <div class="col-sm-12 text-center">

            <div class="jumbotron">

                @if(Session::has('message'))
                    <div class="alert alert-dismissible alert-success">
                        <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
                    </div>

                    <hr>
                @endif

                <h2>Witaj na stronie!</h2>
                <h4>Jeżeli jesteś zainteresowany współpracą, masz pytania dotyczące działania serwisu, wiesz jak można ulepszyć ten serwis lub znalazłeś jakiś błąd na stronie, napisz!</h4>
                <p>Strona ciągle jest rozwijana, więc każda sugestia będzie dla mnie cenną wskazówką.</p>
                <p class="text-muted">Formularz jest anonimowy</p>

                <div class="input-group" style="margin-top: 30px; width: 50%; margin-left: 25%">
                    {{ Form::open(['url' => '/sendMailToAuthor', 'style' => 'width: 100%; position:relative; display: table;']) }}

                    <label for="textArea" class="control-label col-xs-12">Twój adres e-mail (opcjonalnie)</label>
                    <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-lg-offset-1">
                        {{ Form::email('email', null, array('class' => 'form-control', 'style' => 'text-align: center')) }}
                    </div>
                    </div>

                    <label for="textArea" class="control-label col-xs-12">Opis</label>
                    <div class="col-lg-12">
                    <div class="form-group col-lg-10 col-lg-offset-1" >
                        {{ Form::textarea('text', null, array('class' => 'form-control', 'required' => 'required', 'rows' => 3 ,'style' => 'text-align: center; resize: vertical')) }}
                    </div>
                    </div>

                    {{ Form::submit('Wyślij', ['class'=>'btn btn-primary']) }}

                    {{ Form::close() }}
                </div>

            </div>

        </div>
    </div>



@stop

</body>