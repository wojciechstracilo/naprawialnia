@component('mail::message')
# Naprawa Twojego sprzętu w firmie {{ $content['company']->name }} rozpoczęła się

Jeżeli chcesz sprawdzić obecny status naprawy:

@component('mail::button', ['url' => $content['redirect']])
Kliknij tutaj
@endcomponent

lub wejdź na stronę <a href="http://127.0.0.1:8000">naprawialnia.pl</a>, wpisz następujące dane:
Identyfikator naprawy: <b>{{$content['order']->token}}</b><br>
Nazwisko: <b>{{$content['customer']->lastname}}</b><br>
i kliknij "Sprawdź".


Mail wysłany automatycznie przez serwis,<br>
{{ config('app.name') }}.pl
@endcomponent
