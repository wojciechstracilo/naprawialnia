@component('mail::message')
# {{ $content['customer']->name }} {{ $content['customer']->lastname }}  zadał pytanie dotyczące naprawy

@component('mail::panel')
    {{ $content['question'] }}
@endcomponent

@if( ! empty($content['infoBoss']))
    {{ $content['infoBoss'] }}
@endif

@component('mail::button', ['url' => 'mailto:'.$content['customer']->email.
           '?subject=Odpowiedź na pytanie dotyczące naprawy '.$content['order']->name.
            '&body=%0A%0A%0AOdpowiedź na pytanie o treści:%0A"'.$content['question'].'"'])
    Odpowiedz
@endcomponent

Mail wysłany automatycznie przez serwis,<br>
{{ config('app.name') }}.pl
@endcomponent
