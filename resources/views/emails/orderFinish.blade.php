@component('mail::message')
# Twoja naprawa zakończyła się!

Naprawa Twojego sprzętu w serwisie {{ $content['company']->name }} zakończyła się i możesz go odebrać.

@component('mail::button', ['url' => $content['redirect']])
Zobacz historię naprawy
@endcomponent

Mail wysłany automatycznie przez serwis,<br>
{{ config('app.name') }}.pl
@endcomponent
