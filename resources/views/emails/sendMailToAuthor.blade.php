@component('mail::message')
# Pytanie z formularza kontaktowego na stronie

Treść wiadomości

@component('mail::panel')
    {{ $content['text'] }}
@endcomponent


@if( ! empty($content['email']))
@component('mail::button', ['url' => 'mailto:'.$content['email'].
           '?subject=Odpowiedź na pytanie -  Naprawialnia.pl'.
            '&body=%0A%0A%0AOdpowiedź na pytanie o treści:%0A"'.$content['text'].'"'])
Odpowiedz
@endcomponent
@endif


Mail wysłany automatycznie przez serwis,<br>
{{ config('app.name') }}.pl
@endcomponent
