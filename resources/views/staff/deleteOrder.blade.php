@extends('master')

@section('title')
    Usuwanie zamówienia
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/confirmDeletedOrder', 'method' => 'post', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Na pewno chcesz usunąć naprawę?</h3>
            <h4 class="text-danger">UWAGA! Po usunięciu naprawy, nie będzie możliwości jej odzyskania</h4>
            <hr>
            {{ Form::hidden('order_id', $order->order_id) }}


            <h4><b>Nazwa naprawy:</b> {{ $order->name }}</h4>
            @if(Auth::user()->status == 'admin')
                <p><b>Firma:</b> {{ \App\Company::find($order->company_id)->name }}</p>
            @endif
            <p><b>Status:</b> {{ \App\Status::find($order->status_id)->name }}</p>
            <p><b>Imię i nazwisko
                    klienta:</b> {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}
            </p>


            <hr>
            <div class="col-xs-12"><h3>Aby usunąć naprawę, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label col-sm-12 control-label">Hasło</label>

                <div class="form-group col-sm-6  col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label col-sm-12 control-label">Powtórz hasło</label>

                <div class="form-group col-sm-6  col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">

                <div class="col-xs-12">
                    {{ Form::hidden('back_to', url()->previous()) }}
                    {!! Form::submit( 'Usuń', ['class' => 'btn btn-danger', 'name' => 'submit'])!!}
                    {{ Form::close() }}

                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>


    </div>
@stop