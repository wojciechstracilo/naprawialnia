@extends('master')

@section('title')
    Zmiana rachunku
@stop

@section('content')


    <div class="jumbotron">

        <div>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Zmień wartość tego wpisu</h3>

            {{ Form::open(['url' => '/saveEditedBill', 'class'=>'form-inline', 'style' => 'width: 100%; position:relative; display: table;']) }}
            {{ Form::hidden('bill_id', $bill->bill_id) }}


            <h4>Zmień nazwę oraz cenę</h4>
            <div>
                <input style="text-align: center" type="text" class="form-control" name="name" id="name"
                       value="{{ $bill->name }}">
                <input style="text-align: center" type="number" step="0.01" min="0.01" class="form-control" name="cost"
                       id="cost" value= {{ $bill->cost }}>

                <div class="col-lg-12" style="margin-top: 5px">
                    {!! Form::submit( 'Zapisz', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>
            {{ Form::close() }}

        </div>


    </div>

@stop