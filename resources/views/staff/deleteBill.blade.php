@extends('master')

@section('title')
    Usuwanie wpisu
@stop

@section('content')


    <div class="jumbotron col-sm-12 text-center">


                <fieldset>
                    <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Czy na pewno chcesz usunąć ten wpis z rachunku naprawy <b>{{$order->name}}</b>?</h3>

                <div class="col-lg-12">
                    <div class="col-lg-8 col-lg-offset-2">
                        <table class="table table-striped table-hover" style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">

                                <tr>
                                    <td> {{ $bill->name }} </td>
                                    <td> {{ $bill->cost }} zł</td>
                                </tr>
                        </table>
                    </div>
                </div>

                    {{ Form::open(['url' => '/saveDeletedBill', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

                    {{ Form::hidden('bill_id', $bill->bill_id) }}

                    <div class="form-group">
                        <div class="col-lg-12">
                            {!! Form::submit( 'Tak', ['class' => 'btn btn-danger', 'name' => 'submit'])!!}
                            <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>


                        </div>
                    </div>

                </fieldset>
                {{ Form::close() }}



    </div>
@stop