@extends('master')

@section('title')
    Zmiana hasła
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-danger">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        {{ Form::open(['url' => '/saveEditedPassword', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}


        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Edytuj hasło</h3>

            <div class="col-xs-12">
                <hr>
            </div>

            <label for="textArea" class="control-label col-sm-12">Wpisz obecne hasło</label>
            <div class="form-group{{ $errors->has('oldPassword') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="oldPassword" type="password" class="form-control"
                           name="oldPassword" required>

                    @if ($errors->has('oldPassword'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('oldPassword') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Wpisz nowe hasło</label>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Powtórz nowe hasło</label>
            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Zmień hasło', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}
                    <a class="btn btn-default" href="/account">Anuluj</a>
                </div>
            </div>

        </fieldset>


    </div>

@stop