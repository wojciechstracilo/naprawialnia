@extends('master')

@section('title')
    Edytuj swoje dane
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-danger">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        {{ Form::open(['url' => '/saveEditedAccount', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}


        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Edytuj swoje dane</h3>
            <hr>

            <label for="textArea" class="control-label col-sm-12">Imię</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('name', $user->name, array('class' => 'form-control', 'required' => 'required', 'style' => 'text-align: center')) }}
            </div>

            <label for="textArea" class="control-label col-sm-12">Nazwisko</label>
            <div class="form-group col-sm-6 col-sm-offset-3">
                {{ Form::text('lastname', $user->lastname, array('class' => 'form-control', 'required' => 'required', 'style' => 'text-align: center')) }}
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone" class="control-label col-sm-12 control-label">Telefon</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="phone" max="999999999" type="number" class="form-control"
                           name="phone" value="{{ $user->phone }}" required autofocus>

                    @if ($errors->has('phone'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label col-sm-12 control-label">Adres e-mail</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="email" type="email" class="form-control"
                           name="email" value="{{ $user->email }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-xs-12"><h3>Aby zmienić dane, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label col-sm-12 control-label">Hasło</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label col-sm-12 control-label">Powtórz hasło</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}
                    <a class="btn btn-default" href="/account">Anuluj</a>
                </div>
            </div>

        </fieldset>

    </div>

@stop