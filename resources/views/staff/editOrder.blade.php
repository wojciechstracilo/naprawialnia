@extends('master')

@section('title')
    Edycja naprawy
@stop

@section('content')


    <div class="jumbotron col-sm-12 text-center">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Nazwa</h3>
        <h3>{{ $order->name }}</h3>

        <a class="btn btn-info" href="/editOrderName/{{$order->order_id}}">Edytuj nazwę naprawy</a>


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dane klienta i identyfikator naprawy</h3>
        <h4><b>Imię i nazwisko:</b> {{ $order->customer->name }} {{ $order->customer->lastname }}</h4>
        <h4><b>Identyfikator naprawy:</b> {{ $order->token }}</h4>
        <p><b>Telefon:</b> {{ $order->customer->phone }}</p>
        <p><b>E-mail:</b> {{ $order->customer->email }}</p>
        <p><b>Adres:</b> {{ $order->customer->address }}</p>

        {{ Form::open(['url' => ['editCustomer', $order->customer->customer_id], 'method' => 'get']) }}

        {{ Form::hidden('order_id', $order->order_id) }}
        {{ Form::submit('Edytuj dane', ['class'=>'btn btn-info']) }}

        {{ Form::close() }}

        @if((Auth::user()->status === 'boss') || (Auth::user()->status === 'admin'))
            @if($order->employee_id === null)
                <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Brak pracownika nadzorującego</h3>
            @else
                @php($employee = \App\User::find($order->employee_id))
                <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Pracownik nadzorujący</h3>
                <h3>{{$employee->name}} {{$employee->lastname}}</h3>
            @endif
            <a class="btn btn-info" href="/editEmployeeOrder/{{$order->order_id}}">Zmień</a>
        @endif


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Koszty naprawy</h3>

        <div class="col-lg-8 col-lg-offset-2">
            <table class="table table-striped table-hover"
                   style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                @foreach($bills as $bill)
                    <tr>
                        <td> {{ $bill->name }} </td>
                        <td> {{ str_replace('.', ',', $bill->cost) }} zł</td>
                        <td class="col-md-3">

                            <a class="btn btn-info btn-xs" href="/editBill/{{$bill->bill_id}}">Edytuj</a>
                            <a class="btn btn-danger btn-xs" href="/deleteBill/{{$bill->bill_id}}">Usuń</a>

                        </td>

                    </tr>
                @endforeach
                <tr style="background-color: #98cbe8">
                    <th style="text-align: center">Suma</th>
                    <th style="text-align: center">{{ str_replace('.', ',', $sum ) }}
                        zł
                    </th>
                    <th></th>
                </tr>
            </table>
        </div>


        {{ Form::open(['url' => '/addItemToBill', 'class'=>'form-inline', 'style' => 'width: 100%; position:relative; display: table;']) }}
        {{ Form::hidden('order_id', $order->order_id) }}
        <h4>Dodaj produkt/usługę do listy</h4>

        <input style="text-align: center" type="text" class="form-control" minlength="5" maxlength="30" name="name" placeholder="Produkt/usługa">
        <input style="text-align: center" type="number" step="0.01" min="0.01" max="1000000" class="form-control" name="cost"
               placeholder="Kwota">
        <button class="btn btn-primary" type="submit">Dodaj</button>
        {{ Form::close() }}


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Status</h3>
        <div class="text-muted">
            Ostatnia zmiana: {{ $order->updated_at }}
        </div>
        <h2 style="margin-top: 3px; margin-bottom: 15.5px;"><span
                    class="label label-{{ \App\Status::find($order->status_id)->name === "Gotowy do odbioru" ? "success" : "warning" }}">{{ \App\Status::find($order->status_id)->name }}</span>
        </h2>

        <hr>
        <h4>Zmień status</h4>
        {{ Form::open(['url' => '/updateStatus', 'class'=>'form-inline', 'style' => 'width: 100%; position:relative; display: table; text-align-last: center;']) }}
        {{ Form::hidden('order_id', $order->order_id) }}
        <div class="input-group">

            <select class="form-control" name="newStatus">
                <option>Przyjęto do naprawy</option>
                <option>Diagnozowanie</option>
                <option>Oczekiwanie na części</option>
                <option>Naprawa</option>
                <option>Gotowy do odbioru</option>
            </select>

            <span class="input-group-btn">
                                <button class="btn btn-primary" type="submit">Zmień</button>
                            </span>
        </div>
        {{ Form::close() }}


        {{ Form::open(['url' => '/newPost', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <div class="col-lg-12">
                <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dodaj nowy wpis</h3>
            </div>
            {{ Form::hidden('order_id', $order->order_id) }}

            <div class="form-group col-md-8 col-md-offset-2">
                <div>
                    <textarea class="form-control" minlength="10" maxlength="50" rows="2" name="text"
                              style="resize: vertical; text-align: center; margin-top: 15px" required></textarea>

                </div>
            </div>


            <div class="form-group">

                <div class="col-md-12">
                    <button type="reset" class="btn btn-default">Wyczyść</button>
                    <button class="btn btn-primary">Dodaj</button>
                </div>
            </div>


        </fieldset>
        {{ Form::close() }}

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Historia naprawy</h3>
        @foreach($posts as $post)
            <hr>

            <div class="text-muted">
                {{ $post->created_at }}
            </div>
            <h3  style="margin-top: 5px">{{ $post->text }}</h3>
            <p>
                <a class="btn btn-info" href="/editPost/{{$post->post_id}}">Edytuj</a>
                <a class="btn btn-danger" href="/deletePost/{{$post->post_id}}">Usuń</a>
            </p>
        @endforeach


    </div>
@stop