@extends('master')

@section('title')
    Usuwanie wpisu
@stop

@section('content')


    <div class="jumbotron col-sm-12 text-center">


        <fieldset>


            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Czy na pewno chcesz usunąć ten wpis z naprawy <b>{{$order->name}}</b>?</h3>
            <div class="text-muted">
                <small>{{ $post->created_at }}</small>
            </div>
            <h4>{{ $post->text }}</h4>
            {{ Form::open(['url' => '/saveDeletedPost', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

            {{ Form::hidden('post_id', $post->post_id) }}

            <div class="form-group">
                <div class="col-lg-12">
                    {!! Form::submit( 'Tak', ['class' => 'btn btn-danger', 'name' => 'submit'])!!}
                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>

    </div>
@stop