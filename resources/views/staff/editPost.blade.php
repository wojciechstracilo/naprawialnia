@extends('master')

@section('title')
    Zmiana wpisu
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/saveEditedPost', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Zmień ten wpis</h3>
            {{ Form::hidden('post_id', $post->post_id) }}

            <div class="form-group col-md-8 col-md-offset-2">
                <textarea class="form-control" rows="4" id="textArea" name="text"
                          style="resize: vertical; text-align: center">{{$post->text}}</textarea>

            </div>


            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>
        {{ Form::close() }}


    </div>
@stop