@extends('master')

@section('title')
    Edycja dancyh klienta
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/saveEditedCustomer', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}


        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Edytuj dane klienta</h3>
            <hr>
            {{ Form::hidden('customer_id', $customer->customer_id) }}

            @if(Auth::user()->status === 'employee')
                {{ Form::hidden('order_id', $order_id) }}
            @endif
            {{ Form::hidden('back_to', url()->previous()) }}


            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-12 control-label">Imię</label>

                <div class="form-group col-md-6 col-md-offset-3">
                    <input id="name" maxlength="30" style="text-align:center" type="text"
                           class="form-control" name="name"
                           value="{{  (old('name') ? old('name') : $customer->name) }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                <label for="lastname" class="col-md-12 control-label">Nazwisko</label>

                <div class="form-group col-md-6 col-md-offset-3">
                    <input id="lastname" maxlength="30" style="text-align:center" type="text"
                           class="form-control" name="lastname"
                           value="{{  (old('lastname') ? old('lastname') : $customer->lastname) }}" required autofocus>

                    @if ($errors->has('lastname'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="address" class="col-md-12 control-label">Adres</label>

                <div class="form-group col-md-6 col-md-offset-3">
                    <input id="address" style="text-align:center" maxlength="100" type="text"
                           class="form-control" name="address"
                           value="{{ (old('address') ? old('address') : $customer->address) }}" required autofocus>

                    @if ($errors->has('address'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-12 control-label">E-mail</label>

                <div class="form-group col-md-6 col-md-offset-3">
                    <input id="email" style="text-align:center" maxlength="100" type="mail"
                           class="form-control" name="email"
                           value="{{ (old('email') ? old('email') : $customer->email) }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <label for="phone" class="col-md-12 control-label">Telefon</label>

                <div class="form-group col-md-6 col-md-offset-3">
                    <input id="phone" style="text-align:center" maxlength="9" type="number"
                           class="form-control" name="phone"
                           value="{{ (old('phone') ? old('phone') : $customer->phone) }}" required autofocus>

                    @if ($errors->has('phone'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-xs-12"><h3>Aby zmienić dane klienta, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <label for="textArea" class="control-label col-sm-12">Wpisz hasło</label>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Powtórz hasło</label>
            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    <a class="btn btn-default" href="{{url()->previous()}}">Anuluj</a>

                </div>
            </div>

        </fieldset>

        {{ Form::close() }}


    </div>

@stop