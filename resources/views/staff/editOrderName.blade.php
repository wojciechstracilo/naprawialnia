@extends('master')

@section('title')
    Edycja nazwy zamówienia
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/saveEditedOrderName', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Edytuj nazwę naprawy</h3>
            {{ Form::hidden('order_id', $order->order_id) }}

            <div class="form-group col-md-8 col-md-offset-2">
                <textarea minlength="5" maxlength="50" class="form-control" rows="3" id="textArea" name="newName"
                          style="resize: vertical; text-align: center" required>{{$order->name}}</textarea>
            </div>

            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>
        {{ Form::close() }}


    </div>
@stop