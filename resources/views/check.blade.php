@extends('master')

@section('title')
    Twoja naprawa
@stop

@section('content')
    <div class="jumbotron col-sm-12 text-center">

        @if(Session::has('message'))
            <div class="alert alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Nazwa</h3>
        <h3>{{ $order->name }}</h3>

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Koszty naprawy:</h3>


        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-striped table-hover"
                       style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                    @foreach($bills as $bill)
                        <tr>
                            <td> {{ $bill->name }} </td>
                            <td> {{ $bill->cost }} zł</td>

                        </tr>
                    @endforeach
                    <tr style="background-color: #98cbe8">
                        <th style="text-align: center">Suma</th>
                        <th style="text-align: center">{{ $sum }} zł</th>
                    </tr>
                </table>
            </div>

            <div style="width: 100%; display: table">

                <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Status</h3>
                <div class="text-muted">
                    Ostatnia zmiana: {{ $order->updated_at }}
                </div>
                <h2 style="margin-top: 3px; margin-bottom: 15.5px;"><span
                            class="label label-{{ \App\Status::find($order->status_id)->name === "Gotowy do odbioru" ? "success" : "warning" }}">{{ \App\Status::find($order->status_id)->name }}</span>
                </h2>
            </div>

            </fieldset>

            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Historia naprawy:</h3>

            @foreach($posts as $post)

                <hr>
                <div class="text-muted">
                    {{ $post->created_at }}
                </div>
                <h3 style="margin-top: 5px">{{ $post->text }}</h3>
            @endforeach

            <hr>
            {{ Form::open(['url' => '/sendQuestionToOrder', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

            {{ Form::hidden('order_id', $order->order_id) }}
            {{ Form::hidden('lastname', $order->customer->lastname) }}
            <h4><b>Jeżeli masz pytania odnośnie naprawy, pisz. Odpowiemy najszybciej jak to możliwe.</b></h4>
            <p>Odpowiedź zostanie wysłana na Twój e-mail podany w serwisie</p>

            <div class="col-xs-12">
                <div class="col-xs-8 col-xs-offset-2">
                    <textarea class="form-control" rows="4" id="question" name="question"
                              style="resize: vertical; text-align: center" required minlength="25"
                              maxlength="1400"></textarea>
                </div>
            </div>
            {!! Form::submit( 'Wyślij pytanie', ['class' => 'btn btn-primary', 'style' => 'margin-top:15px', 'name' => 'submit'])!!}

            {{ Form::close() }}


        </div>
    </div>


@stop