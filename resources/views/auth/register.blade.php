@extends('master')

@section('title')
    Rejestracja
@stop


@section('content')
    <div class="jumbotron">

        <h3 style="margin-bottom: 40px" class="col-md-10 col-md-offset-1">Aby zarejestrować firmę musisz jednocześnie założyć swoje konto użytkownika. Konto to będzie posiadało status szefa firmy.</h3>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #2c3e50; color: white">Zarejestruj się</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <h4 class="col-md-offset-2">Twoje dane</h4>

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Imię</label>

                                    <div class="col-md-6">
                                        <input id="name" minlength="4" maxlength="30" style="text-align:center" type="text" class="form-control"
                                               name="name" value="{{ old('name') }}" required>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                                    <label for="lastname" class="col-md-4 control-label">Nazwisko</label>

                                    <div class="col-md-6">
                                        <input id="lastname"  minlength="4" maxlength="30" style="text-align:center" type="text" class="form-control"
                                               name="lastname" value="{{ old('lastname') }}" required>

                                        @if ($errors->has('lastname'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Adres e-mail</label>

                                    <div class="col-md-6">
                                        <input id="email" maxlength="50" style="text-align:center" type="email" class="form-control"
                                               name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone" class="col-md-4 control-label">Telefon</label>

                                    <div class="col-md-6">
                                        <input id="phone" max="999999999" style="text-align:center" type="number" class="form-control"
                                               name="phone" value="{{ old('phone') }}" required>

                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Hasło</label>

                                    <div class="col-md-6">
                                        <input id="password" minlength="6" maxlength="180" style="text-align:center" type="password"
                                               class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-4 control-label">Powtórz hasło</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm"  minlength="6" maxlength="180" style="text-align:center" type="password"
                                               class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>


                                <hr>

                                <h4 class="col-md-offset-2">Dane firmy</h4>

                                <div class="form-group{{ $errors->has('companyName') ? ' has-error' : '' }}">
                                    <label for="companyName" class="col-md-4 control-label">Nazwa firmy</label>

                                    <div class="col-md-6">
                                        <input id="companyName" minlength="6" maxlength="50" style="text-align:center" type="text"
                                               class="form-control" name="companyName" value="{{ old('companyName') }}"
                                               required autofocus>

                                        @if ($errors->has('companyName'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('companyName') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('companyAddress') ? ' has-error' : '' }}">
                                    <label for="companyAddress" class="col-md-4 control-label">Adres firmy</label>

                                    <div class="col-md-6">
                                        <input id="companyAddress" minlength="6" maxlength="100" style="text-align:center" type="text"
                                               class="form-control" name="companyAddress"
                                               value="{{ old('companyAddress') }}" required autofocus>

                                        @if ($errors->has('companyAddress'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('companyAddress') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('companyPhone') ? ' has-error' : '' }}">
                                    <label for="companyPhone" class="col-md-4 control-label">Telefon do firmy</label>

                                    <div class="col-md-6">
                                        <input id="companyPhone" style="text-align:center" max="999999999" type="number"
                                               class="form-control" name="companyPhone"
                                               value="{{ old('companyPhone') }}" required autofocus>

                                        @if ($errors->has('companyPhone'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('companyPhone') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                    <label for="description" class="col-md-4 control-label">Krótki opis firmy</label>

                                    <div class="col-md-6">
                                        <textarea id="description" style="text-align:center; resize: vertical"
                                                 minlength="" maxlength="200" class="form-control" name="description"
                                                 required autofocus>{{ old('description') }}</textarea>

                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Zarejestruj
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
