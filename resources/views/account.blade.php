@extends('master')

@section('title')
    Twoje konto
@stop

@section('content')
    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Twoje dane profiowe</h3>

        <div class="list-group">
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Imię</h4>
                <p class="list-group-item-text">{{  Auth::user()->name }}</p>
            </a>
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Nazwisko</h4>
                <p class="list-group-item-text">{{ Auth::user()->lastname  }}</p>
            </a>
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Mail</h4>
                <p class="list-group-item-text">{{ Auth::user()->email }}</p>
            </a>
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Telefon</h4>
                <p class="list-group-item-text">{{ Auth::user()->phone }}</p>
            </a>

            <p style="margin-top: 10px">
            <a class="btn btn-info" href="/editAccount">Edytuj dane</a>
            <a class="btn btn-primary" href="/editPassword">Zmień hasło</a>
            </p>

        </div>
    </div>
@endsection
