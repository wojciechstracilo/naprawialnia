@extends('master')

@section('title')
    Edycja osobę nadzorującą
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        {{ Form::open(['url' => '/saveEditedEmployeeOrder', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Zmień pracownika nadzorującego</h3>
        <hr>

        <h4><b>Nazwa:</b> {{$order->name}}</h4>
        @if((Auth::user()->status === 'boss') || (Auth::user()->status === 'admin'))
            @if($order->employee_id === null)
                <h4><b>Brak pracownika nadzorującego</b></h4>
            @else
                <h4><b>Obecnie nadzorujący pracownik:</b> {{$employee->name}} {{$employee->lastname}}</h4>
            @endif
            <hr>
        @endif

        <label for="employee" class="control-label col-xs-12">Wybierz nowego nadzorującego</label>
        <div style="margin-bottom: 0" class="col-xs-12 form-group{{ $errors->has('employee') ? ' has-error' : '' }}">

            <div class="form-group col-xs-6 col-xs-offset-3">
                <select style="text-align-last:center;" class="form-control" name="employee" style="align: center;">
                    @foreach($employees as $employee)
                        <option value="{{$employee->user_id}}">{{ $employee->name }} {{ $employee->lastname }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div style="width: 100%;display: table">
            <hr></div>
        <h4>Aby potwierdzić zmiany, wpisz dwukrotnie swoje hasło</h4>

        <label for="textArea" class="control-label col-xs-12">Wpisz hasło</label>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="form-group col-xs-6 col-xs-offset-3">
                <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                       required>

                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <label for="textArea" class="control-label col-xs-12">Powtórz hasło</label>
        <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
            <div class="form-group col-xs-6 col-xs-offset-3">
                <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                       name="password_confirmation" required>

                @if ($errors->has('password-confirm'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                @endif
            </div>
        </div>


        <div class="form-group">

            <div class="col-xs-12">

                {{ Form::hidden('order_id', $order->order_id) }}
                {{ Form::hidden('back_to', url()->previous()) }}
                {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                {{ Form::close() }}
                <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>
            </div>
        </div>

        </fieldset>


    </div>

@stop