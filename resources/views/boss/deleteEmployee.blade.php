@extends('master')

@section('title')
    Usuwanie pracownika
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/confirmDeletedEmployee', 'method' => 'post', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Na pewno chcesz usunąć tego
                @if(Auth::user()->status == 'admin')
                    użytkownika?
                @else
                    pracownika?</h3>
            @endif
            <h4 class="text-danger">
                @if(Auth::user()->status == 'admin')
                    @if($employee->status == 'employee')
                        WAŻNE! Po usunięciu użytkownika wszystkie prowadzone przez niego naprawy pozostaną bez
                        opieki
                    @endif
                @else
                    WAŻNE! Po usunięciu pracownika wszystkie prowadzone przez niego naprawy pozostaną bez opieki
                @endif

            </h4>
            <hr>
            {{ Form::hidden('back_to', url()->previous()) }}
            {{ Form::hidden('employee_id', $employee->user_id) }}


            <h4><b>Imię i nazwisko
                    @if(Auth::user()->status == 'boss')
                        pracownika:
                    @else
                        użytkownika:
                    @endif
                </b> {{ $employee->name }} {{ $employee->lastname }}</h4>
            @if(Auth::user()->status == 'admin')
                <p><b>Firma: </b> {{$company->name}}</p>
            @endif
            <p><b>Telefon:</b> {{ $employee->phone }}</p>
            <p><b>Email:</b> {{ $employee->email }}</p>
            @if(Auth::user()->status == 'admin')
                @if($employee->status == 'employee')
                    <p><b>Ilość nadzorowanych napraw:</b> {{ $ordersCount }}</p>
                @endif
            @else
                <p><b>Ilość nadzorowanych napraw:</b> {{ $ordersCount }}</p>
            @endif


            <hr>
            <div class="col-xs-12"><h3>Aby usunąć
                    @if(Auth::user()->status == 'admin')
                        użytkownika
                    @else
                        pracownika
                @endif musisz dwukrotnie wpisać swoje hasło</h3></div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label col-sm-12 control-label">Hasło</label>

                <div class="form-group col-sm-6  col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label col-sm-12 control-label">Powtórz hasło</label>

                <div class="form-group col-sm-6  col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">

                <div class="col-md-12">
                    {!! Form::submit( 'Usuń', ['class' => 'btn btn-danger', 'name' => 'submit'])!!}
                    {{ Form::close() }}

                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>


    </div>
@stop