@extends('master')

@section('title')
    Archiwum napraw
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif


        <div style="width: 100%; display: table">
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Wszystkie archiwalne naprawy w Twojej
                firmie:</h3>

            @foreach($orders as $order)

                <hr>
                <h4><b>Nazwa:</b> {{ $order->name }}</h4>
                <p><b>Data dodania:</b> {{ \App\Post::where('order_id', $order->order_id)->min('created_at') }}</p>

                <p><b>Imię i nazwisko
                        klienta:</b> {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}
                </p>
                <p><b>Identyfikator naprawy:</b> {{ $order->token }}</p>

                @if($order->employee_id === null)
                    <p><b>Brak pracownika nadzorującego</b></p>
                @else
                    @php($employee = \App\User::find($order->employee_id))
                    <p><b>Pracownik nadzorujący:</b> {{$employee->name}} {{$employee->lastname}}</p>
                @endif

                <p>
                    <a class="btn btn-info" href="/archiveDetails/{{$order->order_id}}">Szczegóły</a>
                    <a class="btn btn-warning" href="/unarchiveOrder/{{$order->order_id}}">Przywróć</a>
                    <a class="btn btn-danger" href="/deleteOrder/{{$order->order_id}}">Usuń</a>
                </p>

            @endforeach
            <hr>
            {{ $orders->links() }}


        </div>


    </div>

@stop