@extends('master')

@section('title')
    Przywróć naprawę {{$order->name}}
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/activeOrder', 'method' => 'get', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Na pewno chcesz przywrócić tę
                naprawę z archiwum
                @if(Auth::user()->status == 'admin')
                    firmy <b><i>{{$company->name}}</i></b>@endif?</h3>
            {{ Form::hidden('order_id', $order->order_id) }}
            {{ Form::hidden('back_to', url()->previous()) }}
            <hr>
            <h4><b>Nazwa:</b> {{ $order->name }}</h4>
            @if(Auth::user()->status == 'admin')
                <p><b>Firma:</b> {{$company->name}}</p>
            @endif
            <p><b>Pracownik nadzorujący:</b> {{$employee->name}} {{$employee->lastname}}</p>
            <p><b>Status:</b> {{ \App\Status::find($order->status_id)->name }}</p>

            <p><b>Imię i nazwisko
                    klienta:</b> {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}
            </p>
            <hr>
            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Przywróć', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}

                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>


    </div>
@stop