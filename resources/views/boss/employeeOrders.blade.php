@extends('master')

@section('title')
    Naprawy użytkownika
@stop

@section('content')
    <div class="jumbotron">

    <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Zlecenia prowadzone przez pracownika: <b><i>{{ $user->name }} {{ $user->lastname }}</i></b></h3>


    @foreach($orders as $order)
        <hr>

            <h4>Sprzęt: {{ $order->name }}</h4>
            <p>ID zamówienia: {{ $order->order_id }}</p>
            <p>Status: {{ \App\Status::find($order->status_id)->name }}</p>

            <p>Imię i nazwisko klienta: {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}</p>


            <p>
                <a class="btn btn-primary" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
                <a class="btn btn-warning" href="/archiveOrder/{{$order->order_id}}">Archiwizuj</a>
            </p>
    @endforeach

    </div>
@stop