@extends('master')

@section('title')
    Zmiana uprawnień
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/confirmDoEmployee', 'method' => 'post', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Na pewno chcesz zdegradować tego
                @if(Auth::user()->status == 'boss')
                    wspólnika do konta pracownika?
                @else
                    użytkownika do konta pracownika?
                @endif
            </h3>
            <hr>
            {{ Form::hidden('back_to', url()->previous()) }}
            {{ Form::hidden('user_id', $user->user_id) }}


            <h4><b>Imię i nazwisko: </b>{{ $user->name }} {{ $user->lastname }}</h4>
            @if(Auth::user()->status == 'admin')
                <p><b>Firma: </b>{{\App\Company::find($user->company_id)->name}}</p>
            @endif
            <p><b>E-mail:</b> {{ $user->email }}</p>
            <p><b>Telefon:</b> {{ $user->phone }}</p>

            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Potwierdź', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}

                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>


    </div>
@stop