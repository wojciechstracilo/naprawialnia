@extends('master')

@section('title')
    Usuwanie klienta
@stop

@section('content')


    <div class="jumbotron">

        {{ Form::open(['url' => '/confirmDeletedCustomer', 'method' => 'post', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}

        <fieldset>
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Na pewno chcesz usunąć tego klienta?</h3>
            <h4 class="text-danger">UWAGA! Usunięcie klienta spowoduje usunięcie wszystkich zgłoszonych przez niego napraw</h4>
            <hr>
            {{ Form::hidden('back_to', url()->previous()) }}
            {{ Form::hidden('customer_id', $customer->customer_id) }}


            <h4><b>Imię i nazwisko klienta:</b> {{ $customer->name }} {{ $customer->lastname }}</h4>
            @if(Auth::user()->status == 'admin')
                <p><b>Firma: </b>{{\App\Company::find($customer->company_id)->name}}</p>
                @endif
            <p><b>Adres klienta:</b> {{ $customer->address }}</p>
            <p><b>Telefon:</b> {{ $customer->phone }}</p>
            <p><b>E-mail:</b> {{ $customer->email }}</p>
            <p><b>Ilość napraw klienta w systemie:</b> {{ $ordersCount }}</p>


            <div class="col-xs-12">
                <hr>
            </div>
            <div class="col-xs-12"><h3>Aby usunąć klienta, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label col-sm-12 control-label">Hasło</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label col-sm-12 control-label">Powtórz hasło</label>

                <div class="form-group col-sm-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required>

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">

                <div class="col-md-12">

                    {!! Form::submit( 'Usuń', ['class' => 'btn btn-danger', 'name' => 'submit'])!!}
                    {{ Form::close() }}

                    <a class="btn btn-default" href="{{ url()->previous() }}">Anuluj</a>

                </div>
            </div>

        </fieldset>


    </div>
@stop