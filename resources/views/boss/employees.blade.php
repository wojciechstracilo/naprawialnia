@extends('master')

@section('title')
    Pracownicy
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Wspólnicy:</h3>
        @if(count($bosses)== 0)
            <h3>Brak wspólników</h3>
        @else
            <hr>
        @endif
        @foreach($bosses as $boss)
            <h4><b>Nazwisko i imię:</b> {{$boss->lastname}} {{$boss->name}}</h4>
            <p><b>Mail:</b> {{$boss->email}}</p>
            <p><b>Telefon:</b> {{$boss->phone}}</p>

            <a class="btn btn-default" href="/employeeAccount/{{$boss->user_id}}">Profil</a>
            <a class="btn btn-danger" href="/deleteEmployee/{{$boss->user_id}}">Usuń</a>
            <a class="btn btn-warning" href="/doEmployee/{{$boss->user_id}}">Zmień stanowisko</a>
            <hr>

        @endforeach
        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Lista pracowników:</h3>

        <hr>
        @foreach($employees as $employee)
            <h4>Nazwisko i imię: {{$employee->lastname}} {{$employee->name}}</h4>
            <p>Mail: {{$employee->email}}</p>
            <p>Telefon: {{$employee->phone}}</p>

            <a class="btn btn-info" href="/employeeOrders/{{$employee->user_id}}">Naprawy</a>
            <a class="btn btn-primary" href="/employeeAccount/{{$employee->user_id}}">Profil</a>
            <a class="btn btn-danger" href="/deleteEmployee/{{$employee->user_id}}">Usuń</a>
            <a class="btn btn-warning" href="/doBoss/{{$employee->user_id}}">Zmień stanowisko</a>

            <hr>

            <div class="col-xs-12">
            </div>

        @endforeach
        <div style="width: 100%; display: table"></div>

        <h4 style="background-color: #95a5a6; color: white; padding: 10px 0">Możesz dodać kolejnego pracownika:</h4>
        <a class="btn btn-primary" href="/addEmployee">Dodaj pracownika</a>

    </div>
@stop