@extends('master')

@section('title')
    Informacje o firmie
@stop

@section('content')
    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dane Twojej firmy</h3>

        <div class="list-group">
            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Nazwa firmy</h4>
                <p class="list-group-item-text">{{$company->name}}</p>
            </a>
            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Adres</h4>
                <p class="list-group-item-text">{{$company->address}}</p>
            </a>
            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Telefon</h4>
                <p class="list-group-item-text">{{$company->phone}}</p>
            </a>
            <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Opis</h4>
                <p class="list-group-item-text">{{$company->description}}</p>
            </a>

            {{--<button class="btn btn-info" style="margin-top: 10px">Edytuj dane firmy</button>--}}
            <a class="btn btn-info" href="/editCompany" style="margin-top: 10px">Edytuj dane firmy</a>

        </div>
    </div>
@endsection
