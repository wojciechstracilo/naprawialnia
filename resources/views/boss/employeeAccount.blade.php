@extends('master')

@section('title')
    Twoje konto
@stop

@section('content')
    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

        <h3>Dane profiowe pracownika</h3>

        <div class="list-group">
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Imię</h4>
                <p class="list-group-item-text">{{  $user->name }}</p>
            </a>
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Nazwisko</h4>
                <p class="list-group-item-text">{{ $user->lastname  }}</p>
            </a>
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Mail</h4>
                <p class="list-group-item-text">{{ $user->email }}</p>
            </a>
            <a class="list-group-item">
                <h4 class="list-group-item-heading">Telefon</h4>
                <p class="list-group-item-text">{{ $user->phone }}</p>
            </a>


            <a class="btn btn-info" href="/editEmployeeAccount/{{$user->user_id}}" style="margin-top: 10px">Edytuj dane tego pracownika</a>

        </div>
    </div>
@endsection
