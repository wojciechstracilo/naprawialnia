@extends('master')

@section('title')
    Szczegóły zamówienia
@stop

@section('content')


    <div class="jumbotron col-sm-12 text-center">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Nazwa</h3>
        <h3>{{ $order->name }}</h3>

        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Dane klienta</h3>
        <p>Imię i nazwisko: {{ $order->customer->name }} {{ $order->customer->lastname }}</p>
        <p>Telefon: {{ $order->customer->phone }}</p>
        <p>Mail: {{ $order->customer->email }}</p>


        <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Koszty naprawy:</h3>

        <div class="col-lg-12">
            <div class="col-lg-8 col-lg-offset-2">
                <table class="table table-striped table-hover"
                       style="border: 1px solid rgba(0, 0, 0, 0.2); border-radius: 6px;">
                    @foreach($bills as $bill)
                        <tr>
                            <td> {{ $bill->name }} </td>
                            <td> {{ str_replace('.', ',', $bill->cost) }} zł</td>
                            <td class="col-md-3">

                            </td>

                        </tr>
                    @endforeach
                    <tr style="background-color: #98cbe8">
                        <th style="text-align: center">Suma</th>
                        <th style="text-align: center">{{ str_replace('.', ',', App\Bill::where('order_id', $order->order_id)->sum('cost') ) }}
                            zł
                        </th>
                        <th></th>
                    </tr>
                </table>
            </div>
        </div>


        <div style="width: 100%; display: table">

            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Status</h3>
            <div class="text-muted">
                Ostatnia zmiana: {{ $order->updated_at }}
            </div>
            <h2 style="margin-top: 3px; margin-bottom: 15.5px;"><span
                        class="label label-{{ \App\Status::find($order->status_id)->name === "Gotowy do odbioru" ? "success" : "warning" }}">{{ \App\Status::find($order->status_id)->name }}</span>
            </h2>


            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Historia naprawy:</h3>
            @foreach($posts as $post)
                <hr>
                <div class="text-muted">
                    {{ $post->created_at }}
                </div>
                <h3 style="margin-top: 5px">{{ $post->text }}</h3>

            @endforeach
            <hr>

        </div>
    </div>
@stop