@extends('master')

@section('title')
    Edycja dancyh klienta
@stop

@section('content')


    <div class="jumbotron">

        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-danger">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>

            <hr>
        @endif

                {{ Form::open(['url' => '/saveEditedEmployeeAccount', 'class'=>'form-group', 'style' => 'width: 100%; position:relative; display: table;']) }}


        <fieldset>
            <div><legend>Edytuj dane pracownika</legend></div>
            <hr>
            <label for="name" class="control-label col-sm-12">Imię</label>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="name" type="text" class="form-control" name="name" value="{{ old('name') === null ? $user->name : old('name') }}" required autofocus >

                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Nazwisko</label>
            <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') === null ? $user->lastname : old('lastname') }}" required autofocus >

                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Mail</label>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="email" type="text" class="form-control" name="email" value="{{ old('email') === null ? $user->email : old('email') }}" required autofocus >

                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Telefon</label>
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') === null ? $user->phone : old('phone') }}" required autofocus >

                </div>
            </div>

            <div class="col-xs-12"><hr></div>
            <div class="col-xs-12"><h3>Aby zmienić dane pracownika, musisz dwukrotnie wpisać swoje hasło</h3></div>

            <label for="textArea" class="control-label col-sm-12">Wpisz hasło</label>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="form-group col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password" type="password" class="form-control" name="password"  required >

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <label for="textArea" class="control-label col-sm-12">Powtórz hasło</label>
            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }}">
                <div class="col-md-6 col-sm-offset-3">
                    <input style="text-align: center" id="password-confirm" type="password" class="form-control" name="password_confirmation" required >

                    @if ($errors->has('password-confirm'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


                <div class="form-group">

                <div class="col-md-12" style="margin-top: 10px">
                    {{ Form::hidden('employee_id', $user->user_id) }}

                    {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                    {{ Form::close() }}
{{--                    {!! Form::button( 'Anuluj', ['class' => 'btn btn-default', 'onclick' => '/account'])!!}--}}
                    <a class="btn btn-default" href="/account">Anuluj</a>
                </div>
            </div>

        </fieldset>





            {{--<div class="col-xs-6">
                {{ Form::text('text', $customer->name, ['class' => 'form-control', 'rows' => 4, 'resize' => 'vertical']) }}
            </div>
        --}}{{--</div>--}}{{--
        <br>

        <div class="form-control">
        <fieldset>
            <div><legend>Edytuj ten wpis</legend></div>
                    <hr>
                    --}}{{--{{ Form::hidden('post_id', $post->post_id) }}--}}{{--

                        <label for="textArea" class="control-label col-sm-12">Opis</label>
                        <div class="form-group col-md-8 col-md-offset-2" >
                            --}}{{--<textarea class="form-control" rows="4" name="text"></textarea>--}}{{--
--}}{{--                            {{ Form::textarea('text', $post->text, ['class' => 'form-control', 'rows' => 4, 'resize' => 'vertical']) }}--}}{{--
                            --}}{{--<textarea class="form-control" rows="4" id="textArea" name="text" style="resize: vertical"> {{ $post->text }} </textarea>--}}{{--

                    </div>


                    <div class="form-group">

                        <div class="col-md-12">

                            {!! Form::submit( 'Zapisz zmiany', ['class' => 'btn btn-primary', 'name' => 'submit'])!!}
                            {!! Form::submit( 'Anuluj', ['class' => 'btn btn-default', 'name' => 'submit'])!!}
                        </div>
                    </div>
        </fieldset>
        </div>--}}






    </div>

@stop