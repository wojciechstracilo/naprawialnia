@extends('master')

@section('title')
    Naprawy klienta
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif


        <div style="width: 100%; display: table">
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Wszystkie naprawy klienta
                <b><i>{{ $customer->name }} {{ $customer->lastname }}</i></b>
                @if(Auth::user()->status === 'admin')
                    w firmie <b><i>{{\App\Company::find($customer->company_id)->name}}</i></b>@endif:</h3>
            <hr>

            @foreach($orders as $order)

                <h4><b>Nazwa:</b> {{ $order->name }}</h4>
                <p><b>Ostatnia modyfikacja:</b> {{ \App\Post::where('order_id', $order->order_id)->max('updated_at') }}
                </p>
                <p><b>Pracownik
                        nadzorujący: </b>{{\App\User::find($order->employee_id)->lastname}} {{\App\User::find($order->employee_id)->name}}
                </p>
                <p><b>Status: </b>{{ \App\Status::find($order->status_id)->name }}</p>


                <p>
                    <a class="btn btn-primary" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
                    <a class="btn btn-warning" href="/archiveOrder/{{$order->order_id}}">Archiwizuj</a>
                    <a class="btn btn-danger" href="/deleteOrder/{{$order->order_id}}">Usuń</a>
                </p>
                <hr>
            @endforeach

            {{ $orders->links() }}

        </div>


    </div>

@stop