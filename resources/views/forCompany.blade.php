<!DOCTYPE html>
@extends('master')

@section('title')
    Dla firm
@stop

<body>


@section('content')
    <div class="row content">

        <div class="col-sm-12 text-center">

            <div class="jumbotron">

                <h2>Witaj!</h2>
                <h4>Jeżeli jesteś właścicielem serwisu naprawczego i chcesz usprawnić jego pracę, załóż konto swojej firmy!</h4>
                <a class="btn btn-info" href="/register">Załóż konto</a>

                <h4>Jeśli jesteś już użytkownikiem naszego serwisu przejdź do strony logowania</h4>
                <a class="btn btn-primary" href="/login">Zaloguj</a>


            </div>

        </div>
    </div>



@stop

</body>