@extends('master')

@section('title')
    Błąd
@stop

@section('content')

    <div class="jumbotron col-sm-12 text-center">
        <h2>Nie masz uprawnień do tej strony, lub zostałeś źle przekierowany. Sprawdź
            @if(Auth::guest())
                czy jesteś zalogowany, i
            @endif
            czy poprawnie wprowadziłeś adres.</h2>
        <h3>W przypadku dalszych problemów skontaktuj się z administratorem strony.</h3>
        <br>
        <a href="/" class="btn btn-primary btn-lg">Przejdź do strony głównej</a>
    </div>

@stop