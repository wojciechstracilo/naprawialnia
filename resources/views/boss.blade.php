@extends('master')

@section('title')
    Widok szefa
@stop

@section('content')


    <div class="jumbotron">


        @if(Session::has('message'))
            <div class="alert alert-dismissible alert-success">
                <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
            </div>
            <hr>
        @endif

        <h4>Panel skrótów</h4>
        <div class="col-xs-12">
            <div class="col-md-8 col-md-offset-2">
                <div class="btn-group btn-group-justified">
                    <a href="/active" class="btn btn-info">Naprawy</a>
                    <a href="/customers" class="btn btn-info">Klienci</a>
                    <a href="/employees/{{Auth::user()->company_id}}" class="btn btn-info">Pracownicy</a>
                    <a href="company" class="btn btn-info">Firma</a>
                </div>
            </div>
        </div>


        <div style="width: 100%; display: table; margin-top: 80px">
            <h3 style="background-color: #95a5a6; color: white; padding: 10px 0">Ostatnio zmodyfikowane naprawy:</h3>
            <hr>
            @foreach($orders as $order)

                <h4><b>Nazwa:</b> {{ $order->name }}</h4>
                <p><b>Ostatnia modyfikacja:</b> {{ $order->updated_at }}</p>
                <p><b>Status:</b> {{ \App\Status::find($order->status_id)->name }}</p>
                <p><b>Imię i nazwisko
                        klienta:</b> {{ \App\Customer::find($order->customer_id)->name }} {{ \App\Customer::find($order->customer_id)->lastname }}</p>
                <p><b>Identyfikator zamówienia:</b> {{ $order->token }}</p>
            <p><b>Pracownik nadzorujący: </b> {{\App\User::find($order->employee_id)->name}} {{\App\User::find($order->employee_id)->lastname}}</p>




                <p>
                    <a class="btn btn-primary" href="/editOrder/{{$order->order_id}}">Szczegóły</a>
                    <a class="btn btn-warning" href="/archiveOrder/{{$order->order_id}}">Archiwizuj</a>
                    <a class="btn btn-danger" href="/deleteOrder/{{$order->order_id}}">Usuń</a>
                </p>
                <hr>
            @endforeach

        </div>


    </div>

@stop