<!DOCTYPE html>
@extends('master')

@section('title')
    Strona główna
@stop

<body>


@section('content')
    <div class="row content">

        <div class="col-sm-12 text-center">

            <div class="jumbotron">

                @if(Session::has('message'))
                    <div class="alert alert-danger">
                        <h4 style="margin: 10px 0">{{ Session::get('message') }}</h4>
                    </div>

                    <hr>
                @endif

                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        <h4 style="margin: 10px 0">{{ Session::get('successMessage') }}</h4>
                    </div>

                    <hr>
                @endif

                <h2>Witaj!</h2>
                <h4>Aby sprawdzić stan twojej naprawy, wpisz identyfikator, który dostałeś/aś w serwisie, oraz swoje
                    nazwisko:</h4>


                <div class="input-group" style="margin-top: 30px; width: 50%; margin-left: 25%">
                    {{ Form::open(['url' => '/check', 'style' => 'width: 100%; position:relative; display: table;']) }}

                    <label for="textArea" class="control-label col-xs-12">Identyfikator naprawy</label>
                    <div class="form-group col-sm-8 col-sm-offset-2">
                        {{ Form::text('token', null, array('class' => 'form-control', 'required' => 'required', 'style' => 'text-align: center')) }}
                    </div>

                    <label for="textArea" class="control-label col-sm-12">Twoje nazwisko</label>
                    <div class="form-group col-sm-8 col-sm-offset-2">
                        {{ Form::text('lastname', null, array('class' => 'form-control', 'required' => 'required', 'style' => 'text-align: center')) }}
                    </div>

                    {{ Form::submit('Sprawdź', ['class'=>'btn btn-primary']) }}

                    {{ Form::close() }}
                </div>
            </div>

        </div>
    </div>

@stop

</body>