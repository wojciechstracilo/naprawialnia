<!DOCTYPE html>
<html lang="pl">
<head>
    @yield('head')
    <title>Naprawialnia - @yield('title')</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}"/>

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- CSRF Token -->
    {{--<meta name="csrf-token" content="{{ csrf_token() }}">--}}
</head>


<body>

<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-main"
                    aria-expanded="false" id="menu-button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Naprawialnia.pl</a>
        </div>

        <div class="navbar-collapse collapse" id="navbar-main" aria-expanded="false">

            <ul class="nav navbar-nav">

                @if (Auth::guest())
                    <li style="float: right;"><a href="/contact">Kontakt</a></li>

                @elseif (Auth::user()->status === 'boss')
                    <li class="dropdown {{ Request::is('addOrder') || Request::is('archive') || Request::is('active') ? 'active' : '' }}">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button">Naprawy <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/addOrder">Dodaj naprawę</a></li>
                            <li><a href="/active">Aktywne naprawy</a></li>
                            <li><a href="/archive">Archiwum napraw</a></li>
                        </ul>
                    </li>
                    <li class="{{ Request::is('customers') ? 'active' : '' }}"><a
                                href="/customers">Klienci<span class="sr-only">(current)</span></a></li>

                    <li class="dropdown {{ Request::is('addEmployee') || Request::is('employees/*') ? 'active' : '' }}">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button">Pracownicy <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/employees/{{Auth::user()->company_id}}">Lista pracowników</a></li>
                            <li><a href="/addEmployee">Dodaj pracownika</a></li>
                        </ul>
                    </li>

                    <li class="{{ Request::is('company') ? 'active' : '' }}"><a
                                href="/company">Firma<span
                                    class="sr-only">(current)</span></a></li>



                @elseif (Auth::user()->status === 'employee')
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="/">Naprawy<span
                                    class="sr-only">(current)</span></a></li>
                    <li class="{{ Request::is('addOrder') ? 'active' : '' }}"><a href="/addOrder">Dodaj naprawę<span
                                    class="sr-only">(current)</span></a></li>


                @elseif (Auth::user()->status === 'admin')

                    <li class="dropdown {{ Request::is('allOrders') || Request::is('allArchiveOrders') ? 'active' : '' }}">
                        <a href="/companies" class="dropdown-toggle" data-toggle="dropdown" role="button">Naprawy <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/allOrders">Aktywne naprawy</a></li>
                            <li><a href="/allArchiveOrders">Archiwum napraw</a></li>
                        </ul>
                    </li>


                    <li class="{{ Request::is('allUsers') ? 'active' : '' }}"><a
                                href="/allUsers">Użytkownicy<span class="sr-only"></span></a></li>

                    <li class="{{ Request::is('allCustomers') ? 'active' : '' }}"><a
                                href="/allCustomers">Klienci<span class="sr-only"></span></a></li>

                    <li class="dropdown {{ Request::is('companies') || Request::is('addCompany') ? 'active' : '' }}">
                        <a href="/companies" class="dropdown-toggle" data-toggle="dropdown" role="button">Firmy <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/companies">Lista firm</a></li>
                            <li><a href="/addCompany">Dodaj firmę</a></li>
                        </ul>
                    </li>

            @endif

            </ul>

            <ul class="nav navbar-nav navbar-right">

                @if (Auth::guest())
                    <li><a href="{{ route('forCompany') }}">Dla firm</a></li>
                @else
                    <li class="dropdown {{ Request::is('account') ? 'active' : '' }}">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} {{ Auth::user()->lastname }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{ url('account') }}">
                                    Profil
                                </a>
                            </li>


                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Wyloguj
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>


                        </ul>
                    </li>
            @endif
            </ul>
        </div>
    </div>
</div>


<div class="container text-center" style="padding-top: 81px;">
    @yield('content')
</div>

<div class="panel-footer" style="text-align: center;">Copyright Rzeszów 2018 @if(Auth::check()) ||  <a style="color: #2c3e50" href="/contact">Kontakt</a>@endif</div>
</body>
</html>