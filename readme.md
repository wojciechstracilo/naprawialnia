## Projekt serwisu wspomagającego zarządzanie naprawami

Projekt uczelniany, który był ograniczony czasowo i służył do nauki frameworku Laravel. Nie został dopracowany,
dlatego istnieje możliwość jego rozbudowy i optymalizacji.

## Skrócona intrukcja uruchomienia
W razie problemów, instrukcja uruchomienia projektu znajduje się w dokumentacji
frameworku Laravel [Laravel](https://laravel.com/docs/5.4)

- Aby poprawnie uruchomić projekt, potrzebny jest przede wszystkim zainstalowany
[PHP](http://php.net/downloads.php) oraz
 [Composer](https://getcomposer.org)
 - W folderze projektu uruchamiamy w konsoli polecenie: _composer install_
 - W konsoli generujemy unikatowy klucz aplikacji: _php artisan key:generate_
 - Tworzymy bazę danych MySQL i konfigurujemy połączenie w pliku _.env_
 - W konsoli uruchamiamy polecenie _php artisan migrate_, a następnie _php artisan db:seed_
 - Uruchamiamy projekt w kosoli poleceniem: _php artisan serve_. Domyślnie projekt
 można uruchomić pod adresem http://127.0.0.1:8000 w przeglądarce
 
## Dane do logowania
Lista domyślnych kont, dzięki którym można przetestować aplikację:
1. Administrator:
    - E-mail: _admin@yopmail.com_, hasło: _admin_
2. Szef firmy: 
    - E-mail: _pkowalski@yopmail.com_, hasło: _boss_
    -  E-mail: _hzielinska@yopmail.com_, hasło: _boss_
3. Pracownicy:
    - E-mail: _wnowak@yopmail.com_, hasło: _user_
    - E-mail: _mkacprzak@yopmail.com_, hasło: _user_
    - E-mail: _jwilk@yopmail.com_, hasło: _user_
    - E-mail: _mkoziol@yopmail.com_, hasło: _user_
4. Nazwiska klientów i identyfikatory napraw, dzięki którym mogą oni 
sprawdzać statusy swoich napraw:
    - Nazwisko: _Białek_, ID naprawy: _reZCV9RG4u_
    - Nazwisko: _Kot_, ID naprawy: _coueJVzzjZ_
    - Nazwisko: _Nowak_, ID naprawy: _mo6FDU2tch_
    - Nazwisko: _Wiśniewski_, ID naprawy: _8BZcKMRRFm_
   
## Dodatkowe informacje
1. Pewne funkcjonalności aplikacji wykorzystują wysyłanie maili. Aby ta usługa działała poprawnie
    w pliku _.env_ należy podmienić następujący kod konfiguracyjny:
    ```
    MAIL_DRIVER=smtp
    MAIL_HOST=smtp.gmail.com
    MAIL_PORT=465
    MAIL_USERNAME=moje.naprawy@gmail.com
    MAIL_PASSWORD=trudnehaslo
    MAIL_ENCRYPTION=ssl
    ```
    Wysyłanie e-maili wykorzystuje specjalnie stworzone konto GMail. Czasami istnieje potrzeba
    zalogowania się do tego konta (podanymi powyżej danymi) i potwierdzenia zabezpieczeń.
    
    Istnieje oczywiście możliwość skofigurowania własnej usługi pocztowej.
2. Wszystkie konta klientów w systemie wykorzystują darmowe tymczasowe konta mailowe (na które wysyłane są e-maile),
    które można sprawdzić wchodząc na [tę stronę](http://www.yopmail.com/pl/), i wpisując
    odpowiedni przedrostek do adresu _@yopmail.com_
3. Do stworzenia projektu zostały 
wykorzystane następujące repozytoria:
    - [Bootswatch Flatly](https://bootswatch.com/flatly/) - Zmodyfikowany plik CSS frameworku Bootstrap
    - [Laravel Collective](https://laravelcollective.com/docs/5.4/html) - Biblioteka ułatwiająca tworzenie formularzy
    w frameworku Laravel