<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $primaryKey = 'post_id';

    protected $fillable  = [
        'post_id', 'text', 'order_id'
    ];

    /**
     * Funkcja tworzy nowy post i zwraca identyfikator (ID) utworzonego postu
     * @param $text - Tresc postu
     * @param $order_id - Identyfikator (ID) zamowienia, dla ktorego tworozny jest post
     * @return post_id - Funkcja zwraca identyfikator (ID) utworzonego postu
     */
    public static function createPost($text, $order_id){
        $newPost = new Post(['text' => $text, 'order_id' => $order_id]);
        $newPost -> save();

        return $newPost->post_id;
    }

    /**
     * Post nalezy tylko do jednego zamowienia
     */
    public function order(){
        return $this->belongsTo('App\Order', 'order_id');
    }
}
