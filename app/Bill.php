<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{

    protected $primaryKey = 'bill_id';

    protected $fillable  = [
        'bill_id', 'name', 'cost', 'order_id'
    ];

    /**
     * Kazdy rachunek nalezy do jednego zamowienia
     */
    public function order(){
        return $this->belongsTo('App\Order', 'order_id');
    }
}
