<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Company;
use App\Customer;
use App\Mail\NewOrder;
use App\Mail\OrderFinish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Order;
use App\Status;
use App\Post;
use App\Http\Requests\CheckOrderRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\Rule;


class StaffController extends Controller
{


    public function updateStatus(CheckOrderRequest $request)
    {
        if (($request == null) || (Order::find($request->order_id) == null)) {
            abort(401);
        }

        $order_id = $request->input('order_id');
        $newStatus = $request->input('newStatus');

        if ((Order::find($order_id)->company_id === Auth::user()->company_id) || (Auth::user()->status === 'boss' && Order::find($order_id)->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin')) {
            $status_id = Status::getIdByName($newStatus);

            Order::updateStatus($order_id, $status_id);

            $textPost = "Zmieniono status na: " . $newStatus;
            Post::createPost($textPost, $order_id);

            if ($status_id === 5) {
                $order = Order::find($order_id);
                $company = Company::find($order->company_id);
                $customer = Customer::find($order->customer_id);
                $subject = "Naprawa ''$order->name'' zakończyła się";
                $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;

                $content = [
                    'order' => $order,
                    'customer' => $customer,
                    'redirect' => $redirect,
                    'subject' => $subject,
                    'company' => $company
                ];

                Mail::to($customer)->send(new OrderFinish($content));
            }

            if ($status_id === 5) {
                Session::flash('message', 'Zaktualizowano status naprawy i wysłano powiadomienie mailowe klientowi');
            } else {
                Session::flash('message', 'Zaktualizowano status naprawy');
            }
            return redirect()->route('editOrder', ['id' => $order_id]);
        }
    }

    public function editOrder($id)
    {

        if (Order::find($id) === null) {
            abort(404);
        } elseif (Auth::user()->status === 'admin' ||
            (Auth::user()->status === 'boss') && (Auth::user()->company_id === Order::find($id)->company_id) ||
            (Auth::user()->status === 'employee') && (Auth::user()->user_id === Order::find($id)->employee_id)) {
            $order = Order::find($id);
            $bills = Bill::where('order_id', $order->order_id)->get();
            $sum = Bill::where('order_id', $id)->sum('cost');
            $posts = Post::where('order_id', $id)->orderBy('created_at', 'desc')->get();
            return view('staff.editOrder')->with(['order' => $order, 'bills' => $bills, 'sum' => $sum, 'posts' => $posts]);
        } else {
            return redirect('error');
        }

    }

    public function newPost(CheckOrderRequest $request)
    {
        if (($request == null) || (Order::find($request->order_id) == null)) {
            abort(404);
        }

        $order_id = $request->input('order_id');
        if ((Order::find($order_id)->employee_id === Auth::user()->user_id) ||
            (Auth::user()->status === "boss" && (Order::find($order_id)->company_id === Auth::user()->company_id)) ||
            (Auth::user()->status === "admin")) {

            Post::createPost($request->text, $order_id);
            Session::flash('message', 'Dodano nowy wpis');
            return redirect()->route('editOrder', ['id' => $order_id]);
        } else {
            return redirect('error');

        }
    }

    public function editPost($id)
    {
        $post = Post::find($id);

        if ($post === null) {
            abort(404);
        } elseif ($post->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && $post->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin')) {
            return view('staff.editPost')->with('post', $post);
        } else {
            return redirect('error');
        }

    }

    public function deletePost($id)
    {
        $post = Post::find($id);

        if ($post === null) {
            abort(404);
        }

        $order = Order::find($post->order_id);

        if ($post->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && $post->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin')) {
            return view('staff.deletePost')->with(['post' => $post, 'order' => $order]);
        } else {
            return redirect('error');
        }

    }


    public function saveEditedPost(CheckOrderRequest $request)
    {
        $post = Post::find($request->post_id);

        if (($request == null) || ($post == null)) {
            abort(404);
        }

        $text = $request->input('text');
        $order_id = $post->order->order_id;


        if ($post->order->employee_id === Auth::user()->user_id ||
            (Auth::user()->status === 'boss' && $post->order->user->company_id === Auth::user()->company_id) ||
            (Auth::user()->status === 'admin')) {
            Post::where('post_id', $post->post_id)->update(['text' => $text]);
            Session::flash('message', 'Zaktualizowano wpis');
            return redirect()->route('editOrder', ['id' => $order_id]);
        } else {
            return view('error');
        }

    }

    public function saveDeletedPost(CheckOrderRequest $request)
    {
        $post = Post::find($request->post_id);

        if (($request == null) || ($post == null)) {
            abort(404);
        }

        $order_id = $post->order_id;

        if ($request->input('submit') === 'Tak') {
            if ($post->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && $post->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin'))
                \DB::table('posts')->where('post_id', $post->post_id)->delete();
            return redirect()->route('editOrder', ['id' => $order_id]);

        } elseif ($request->input('submit') === 'Nie') {
            if ($post->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && $post->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin'))
                return redirect()->route('editOrder', ['id' => $order_id]);

        } else {
            return redirect('error');
        }

    }

    public function addItemToBill(CheckOrderRequest $request)
    {
        $order_id = $request->input('order_id');

        if (($request == null) || (Order::find($order_id) == null)) {
            abort(404);
        }

        $name = $request->input('name');
        $cost = $request->input('cost');


        if ((Auth::user()->status === 'admin') ||
            (Auth::user()->status === 'boss') && (Auth::user()->company_id === Order::find($order_id)->user->company_id) ||
            (Auth::user()->status === 'employee') && (Auth::user()->user_id === Order::find($order_id)->user->user_id)) {

            $bill = new Bill(["name" => $name, "cost" => $cost, "order_id" => $order_id]);
            $bill->save();
            Session::flash('message', 'Dodano wpis do rachunku');
            return redirect()->route('editOrder', ['id' => $order_id]);

        } else {
            return redirect('error');
        }

    }

    public function editBill($id)
    {
        $bill = Bill::find($id);

        if ($bill == null) {
            abort(404);
        }

        if (Bill::find($id) === null) {
            abort(404);
        } elseif ($bill->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && $bill->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin')) {
            return view('staff.editBill')->with('bill', $bill);
        } else {
            return redirect('error');
        }
    }

    public function deleteBill($id)
    {
        $bill = Bill::find($id);

        if ($bill == null) {
            abort(404);
        }

        $order = Order::find($bill->order_id);


        if ($bill->order->employee_id === Auth::user()->user_id
            || (Auth::user()->status === 'boss' && Order::find($bill->order_id)->company_id === Auth::user()->company_id)
            || (Auth::user()->status === 'admin')) {
            return view('staff.deleteBill')->with(['bill' => $bill, 'order' => $order]);
        } else {
            return redirect('error');
        }
    }

    public function saveEditedBill(CheckOrderRequest $request)
    {
        $bill_id = $request->input('bill_id');

        if (($request == null) || (Bill::find($bill_id) == null)) {
            abort(404);
        }

        $order_id = Bill::find($bill_id)->order_id;
        $name = $request->input('name');
        $cost = $request->input('cost');

        if ($request->input('submit') === 'Zapisz') {
            if (Bill::find($bill_id)->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && Bill::find($bill_id)->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin')) {
                Bill::where('bill_id', $bill_id)->update(['name' => $name, 'cost' => $cost]);
                Session::flash('message', 'Zaktualizowano koszty naprawy');
                return redirect()->route('editOrder', ['id' => $order_id]);
            } else {
                return redirect('error');
            }
        } elseif ($request->input('submit') === 'Anuluj') {
            if (Bill::find($bill_id)->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && Bill::find($bill_id)->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin'))
                return redirect()->route('editOrder', ['id' => $order_id]);

        } else {
            return redirect('error');
        }

    }

    public function saveDeletedBill(CheckOrderRequest $request)
    {
        $bill_id = $request->input('bill_id');

        if (($request == null) || (Bill::find($bill_id) == null)) {
            abort(404);
        }

        $order_id = Bill::find($bill_id)->order_id;

        if ($request->input('submit') === 'Tak') {
            if (Bill::find($bill_id)->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && Bill::find($bill_id)->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin'))
                \DB::table('bills')->where('bill_id', $bill_id)->delete();
            return redirect()->route('editOrder', ['id' => $order_id]);
        } elseif ($request->input('submit') === 'Nie') {
            if (Bill::find($bill_id)->order->employee_id === Auth::user()->user_id || (Auth::user()->status === 'boss' && Bill::find($bill_id)->order->user->company_id === Auth::user()->company_id) || (Auth::user()->status === 'admin'))
                return redirect()->route('editOrder', ['id' => $order_id]);

        } else {
            return redirect('error');
        }

    }


    public function addOrder()
    {
        if (Auth::user()->status == 'admin') {
            return redirect('error');
        } else {
            $customers = \App\Customer::where('company_id', Auth::user()->company_id)->orderBy('lastname', 'asc')->get();
            $employees = \App\User::where('company_id', Auth::user()->company_id)->where('status', 'employee')->get();
        }
        return view('staff.addOrder')->with(['customers' => $customers, 'employees' => $employees]);
    }

    public function account()
    {
        return view('account');
    }


    public function addNewOrder(CheckOrderRequest $request)
    {
        if ($request == null) {
            abort(404);
        }

        if (Auth::user()->status === 'employee') { //jeśli zamówienie dodaje pracownik
            if ($request->radio === 'new') { //tworzenie nowego klienta
                $newCustomerId = Customer::createCustomer($request->customerName, $request->customerLastName, $request->address, $request->email, $request->phone, Auth::user()->company_id);
                $newOrderId = Order::createOrder($request->name, Auth::user()->user_id, $newCustomerId, Auth::user()->company_id);
                Post::createPost('Sprzęt przyjęty do naprawy', $newOrderId);

                $order = Order::find($newOrderId);
                $customer = Customer::find($newCustomerId);
                $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;
                $subject = "Naprawa ''$order->name'' rozpoczęła się";
                $company = Company::find($order->company_id);
                $content = [
                    'order' => $order,
                    'customer' => $customer,
                    'redirect' => $redirect,
                    'subject' => $subject,
                    'company' => $company
                ];
                Mail::to($customer)->send(new NewOrder($content));

                Session::flash('message', "Dodano naprawę i wysłano powiadomienie e-mailowe klientowi");
                return redirect()->route('editOrder', ['id' => $newOrderId]);

            } elseif ($request->radio === 'old') { //wykorzystanie już istniejącego klienta w bazie
                $newOrderId = Order::createOrder($request->name, Auth::user()->user_id, $request->oldCustomer, Auth::user()->company_id);
                Post::createPost('Sprzęt przyjęty do naprawy', $newOrderId);

                $order = Order::find($newOrderId);
                $customer = Customer::find($request->oldCustomer);
                $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;
                $subject = "Naprawa ''$order->name'' rozpoczęła się";
                $company = Company::find($order->company_id);
                $content = [
                    'order' => $order,
                    'customer' => $customer,
                    'redirect' => $redirect,
                    'subject' => $subject,
                    'company' => $company
                ];
                Mail::to($customer)->send(new NewOrder($content));

                Session::flash('message', 'Dodano nową naprawę i wysłano powiadomienie mailowe klientowi');
                return redirect()->route('editOrder', ['id' => $newOrderId]);

            } else { //inne błędy
                return redirect('error');
            }

        } elseif (Auth::user()->status === 'boss') { // jeśli zamówienie dodaje szef firmy
            if ($request->radio === 'new') {
                $newCustomerId = Customer::createCustomer($request->customerName, $request->customerLastName, $request->address, $request->email, $request->phone, Auth::user()->company_id);
                $newOrderId = Order::createOrder($request->name, $request->employee, $newCustomerId, Auth::user()->company_id);
                Post::createPost('Sprzęt przyjęty do naprawy', $newOrderId);

                $order = Order::find($newOrderId);
                $customer = Customer::find($newCustomerId);
                $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;
                $subject = "Naprawa ''$order->name'' rozpoczęła się";
                $company = Company::find($order->company_id);
                $content = [
                    'order' => $order,
                    'customer' => $customer,
                    'redirect' => $redirect,
                    'subject' => $subject,
                    'company' => $company
                ];
                Mail::to($customer)->send(new NewOrder($content));

                Session::flash('message', 'Dodano nową naprawę i wysłano powiadomienie mailowe klientowi');
                return redirect()->route('editOrder', ['id' => $newOrderId]);

            } elseif ($request->radio === 'old') {
                $newOrderId = Order::createOrder($request->name, $request->employee, $request->oldCustomer, Auth::user()->company_id);
                Post::createPost('Sprzęt przyjęty do naprawy', $newOrderId);

                $order = Order::find($newOrderId);
                $customer = Customer::find($request->oldCustomer);
                $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;
                $subject = "Naprawa ''$order->name'' rozpoczęła się";
                $company = Company::find($order->company_id);
                $content = [
                    'order' => $order,
                    'customer' => $customer,
                    'redirect' => $redirect,
                    'subject' => $subject,
                    'company' => $company
                ];
                Mail::to($customer)->send(new NewOrder($content));

                Session::flash('message', 'Dodano nową naprawę i wysłano powiadomienie mailowe klientowi');
                return redirect()->route('editOrder', ['id' => $newOrderId]);

            } else {
                return redirect('error');
            }
        }
    }


    public function editOrderName($id)
    {

        $order = Order::find($id);
        if ($order == null) {
            abort(404);
        }

        if (($order->employee_id === Auth::user()->user_id) || (($order->company->company_id === Auth::user()->company_id) && (Auth::user()->status === 'boss')) || Auth::user()->status === 'admin') {
            return view('staff.editOrderName')->with('order', $order);
        } else {
            return redirect('error');
        }

    }


    public function saveEditedOrderName(CheckOrderRequest $request)
    {
        $order = Order::find($request->order_id);

        if (($request == null) || ($order == null)) {
            abort(404);
        }


        if (($order->employee_id === Auth::user()->user_id) || (($order->company->company_id === Auth::user()->company_id) && (Auth::user()->status === 'boss')) || (Auth::user()->status === 'admin')) {
            $newNameOrderId = Order::updateNameOrder($order, $request->newName);
            Session::flash('message', 'Zmieniono nazwę zamówienia');
            return redirect()->route('editOrder', ['id' => $newNameOrderId]);
        } else {
            return redirect('error');
        }

    }


    public function editCustomer($id, CheckOrderRequest $request)
    {
        $customer = Customer::find($id);
        if (($request == null) || ($customer == null)) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || ((Auth::user()->status === 'boss') && (Auth::user()->company_id === $customer->company_id))) {
            return view('staff.editCustomer')->with('customer', $customer);
        } elseif ((Order::find($request->order_id)->employee_id === Auth::user()->user_id)) {
            return view('staff.editCustomer')->with('customer', $customer)->with('order_id', $request->order_id);
        } else {
            return redirect('error');
        }
    }


    public function saveEditedCustomer(CheckOrderRequest $request)
    {
        if (($request == null) || (Customer::find($request->customer_id) == null)) {
            abort(404);
        }


        $this->validate($request, [
            'password' => 'required|string|confirmed',
            'email' => 'required|email',
            'phone' => 'required|integer',
        ], [], [
            'password' => 'Hasło',
            'email' => 'e-mail',
            'phone' => 'telefon',
        ]);

        $order = Order::find($request->order_id);
        $customer = Customer::find($request->customer_id);


        if (!\Hash::check($request->password, \Auth::user()->password)) {
            $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
            return redirect()->route('editCustomer', ['id' => $request->customer_id])->withInput()->withErrors($errors);
        } else {
            if ((($customer->company_id === Auth::user()->company_id) && (Auth::user()->status === 'boss')) || (Auth::user()->status === 'admin')) {
                Customer::updateCustomer($request->customer_id, $request->name, $request->lastname, $request->address, $request->email, $request->phone);
                Session::flash('message', 'Zmieniono dane klienta');
                return redirect($request->back_to);
            } elseif (($order->employee_id === Auth::user()->user_id)) {
                Customer::updateCustomer($request->customer_id, $request->name, $request->lastname, $request->address, $request->email, $request->phone);
                Session::flash('message', 'Zmieniono dane klienta');
                return redirect($request->back_to);
            } else {
                return redirect('error');
            }
        }
    }

    public function editAccount()
    {
        $user = User::getUser(Auth::user()->user_id);

        return view('staff.editAccount')->with('user', $user);
    }

    public function saveEditedAccount(CheckOrderRequest $request)
    {
        $user = Auth::user()->user_id;
        $this->validate($request, [
            'password' => 'required|string|confirmed',
            'email' => Rule::unique('users')->ignore($user, 'user_id'),
            'phone' =>  Rule::unique('users')->ignore($user, 'user_id'),
        ], [], [
            'password' => 'Hasło'
        ]);

        if (!(\Hash::check($request->password, Auth::user()->password))) { //jeśli hasło jest złe
            $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
            return redirect('editAccount')->withInput()->withErrors($errors);
        } elseif (\Hash::check($request->password, Auth::user()->password)) { //jeśli hasło jest dobre
            User::updateAccount(Auth::user()->user_id, $request->name, $request->lastname, $request->email, $request->phone);
            Session::flash('message', 'Zaktualizowano dane profilowe');
            return redirect('account');

        } else {
            return redirect('error');
        }
    }

    public function editPassword()
    {
        $user = User::getUser(Auth::user()->user_id);

        return view('staff.editPassword')->with('user', $user);
    }


    public function saveEditedPassword(Request $request)
    {

        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        if (!(Hash::check($request->oldPassword, Auth::user()->password))) { //jeśli hasło jest złe
            $errors = new MessageBag(['oldPassword' => 'Hasło nie jest poprawne']);
            return redirect('editPassword')->withErrors($errors);
        } elseif (Hash::check($request->oldPassword, Auth::user()->password)) { //jeśli hasło jest dobre
            User::newPassword(Auth::user()->user_id, $request->password);
            Session::flash('message', 'Hasło zostało zmienione');
            return redirect('account');

        } else {
            return redirect('error');
        }
    }

    public function deleteOrder($id)
    {
        $order = Order::find($id);
        if ((Auth::user()->user_id == $order->employee_id) || (Auth::user()->status === 'admin') || ((Auth::user()->company_id === $order->company_id) && Auth::user()->status == 'boss')) {
            return view('staff.deleteOrder')->with(['order' => $order]);
        } else {
            return redirect('error');
        }
    }

    public function confirmDeletedOrder(Request $request)
    {

        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        $order = Order::find($request->order_id);
        if (($request == null) || ($order == null)) {
            abort(404);
        }


        if ((Auth::user()->status === 'admin')
            || ((Auth::user()->company_id === $order->company_id) && (Auth::user()->status === 'boss'))
            || (Auth::user()->user_id) === $order->employee_id) {
            if (!\Hash::check($request->password, \Auth::user()->password)) {
                $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
                return redirect(url()->previous())->withInput()->withErrors($errors);
            } else {
                $order->delete();
                return redirect($request->back_to)->with('message', 'Naprawa została usunięta');
            }
        } else {
            return redirect('error');
        }
    }

}