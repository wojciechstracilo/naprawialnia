<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Company;
use App\Customer;
use App\Http\Requests\AddEmployeeRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Order;
use App\Http\Requests\CheckOrderRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\MessageBag;


class BossController extends Controller
{

    public function company()
    {
        $company = Company::find(Auth::user()->company_id);
        return view('boss.company')->with('company', $company);
    }


    public function employeeOrders($id)
    {
        $user = User::find($id);

        if ($user == null) {
            abort(404);
        }

        if ($user->company_id === Auth::user()->company_id) {
            $orders = User::find($id)->orders;
            return view('boss.employeeOrders')->with(['user' => $user, 'orders' => $orders]);
        } else {
            return redirect('error');
        }
    }

    public function employeeAccount($id)
    {
        $employee = User::find($id);

        if ($employee == null) {
            abort(404);
        }

        if ($employee->company_id === Auth::user()->company_id) {
            if (Auth::user()->company_id === $employee->company_id) {
                return view('boss.employeeAccount')->with(['user' => $employee]);
            } else {
                return redirect('error');
            }
        } else {
            return redirect('error');
        }
    }


    public function editEmployeeAccount($id)
    {
        $employee = User::find($id);

        if ($employee == null) {
            abort(404);
        }

        if (Auth::user()->company_id === $employee->company_id) {
            return view('boss.editEmployeeAccount')->with(['user' => $employee]);
        } else {
            return redirect('error');
        }
    }

    public function saveEditedEmployeeAccount(CheckOrderRequest $request)
    {

        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        $employee = User::find($request->employee_id);

        if (!\Hash::check($request->password, Auth::user()->password)) {
            $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
            return redirect()->route('editEmployeeAccount', ['id' => $request->employee_id])->withInput()->withErrors($errors);
        } elseif ((Auth::user()->company_id === $employee->company_id) && (\Hash::check($request->password, Auth::user()->password))) {
            User::updateEmployee($request->employee_id, $request->name, $request->lastname, $request->email, $request->phone);
            Session::flash('message', 'Zaktualizowano dane profilowe pracownika');
            return redirect()->route('employeeAccount', ['id' => $request->employee_id]);
        }

    }

    public function archiveOrder($id)
    {
        $order = Order::find($id);

        if ($order == null) {
            abort(404);
        }

        $employee = User::find($order->employee_id);
        if (Auth::user()->status === "admin") {
            $company = Company::find($order->company_id);
            return view('boss.confirmArchiveOrder')->with(['order' => $order, 'employee' => $employee, 'company' => $company]);

        } elseif ($order->company_id === Auth::user()->company_id) {
            return view('boss.confirmArchiveOrder')->with(['order' => $order, 'employee' => $employee]);
        } else {
            return redirect('error');
        }
    }

    public function saveOrderToArchive(CheckOrderRequest $request)
    {
        if ($request == null) {
            abort(404);
        }

        if ((Order::find($request->order_id)->company_id === Auth::user()->company_id) || Auth::user()->status === 'admin') {
            Order::archive($request->order_id);
            $order = Order::find($request->order_id);
            Session::flash('message', "Naprawa ''$order->name'' została przeniesiona do archiwum");
            return redirect($request->back_to);
        } else {
            return redirect('error');
        }
    }

    public function editCompany()
    {
        $company = Company::find(Auth::user()->company_id);
        return view('boss.editCompany')->with(['company' => $company]);

    }

    public function saveEditedCompany(CheckOrderRequest $request)
    {
        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        if ((Auth::user()->status === 'admin')) {
            if (!\Hash::check($request->password, Auth::user()->password)) {
                $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
                return redirect()->route('editCompany', ['id' => $request->company_id])->withInput()->withErrors($errors);
            } else {
                Company::updateCompany($request->company_id, $request->name, $request->address, $request->phone, $request->description);
                Session::flash('message', 'Zaktualizowano dane firmy');
                return redirect()->route('company', ['id' => $request->company_id]);
            }
        } elseif (!\Hash::check($request->password, Auth::user()->password)) {
            $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
            return redirect('editCompany')->with('company', Company::find(Auth::user()->company_id))->withInput()->withErrors($errors);
        } elseif ((\Hash::check($request->password, Auth::user()->password))) {
            Company::updateCompany(Auth::user()->company_id, $request->name, $request->address, $request->phone, $request->description);
            Session::flash('message', 'Zaktualizowano dane firmy');
            return redirect('company');
        } else {
            return redirect('error');
        }
    }

    public function archive()
    {
        $orders = Order::where('company_id', Auth::user()->company_id)->where('active', 'false')->orderBy('order_id', 'desc')->paginate(10);
        return view('boss.archive')->with('orders', $orders);
    }

    public function active()
    {
        $orders = Order::where('company_id', Auth::user()->company_id)->where('active', 'true')->orderBy('order_id', 'desc')->paginate(10);
        return view('boss.active')->with('orders', $orders);
    }

    public function archiveDetails($id)
    {
        $order = Order::find($id);

        if ($order == null) {
            abort(404);
        }

        if ($order->company_id === Auth::user()->company_id) {
            $bills = Bill::where('order_id', $id)->get();
            $posts = Post::where('order_id', $id)->orderBy('created_at', 'desc')->get();
            return view('boss.archiveDetails')->with(['order' => $order, 'bills' => $bills, 'posts' => $posts]);
        }
    }

    public function unarchiveOrder($id)
    {
        $order = Order::find($id);

        if ($order == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || (Order::find($id)->company_id === Auth::user()->company_id)) {
            $company = Company::find($order->company_id);
            $employee = User::find($order->employee_id);

            return view('boss.confirmUnarchiveOrder')->with(['order' => $order, 'company' => $company, 'employee' => $employee]);
        } else {
            return redirect('error');
        }
    }

    public function activeOrder(CheckOrderRequest $request)
    {
        $order = Order::find($request->order_id);

        if ($order == null) {
            abort(404);
        }

        if ((Order::find($request->order_id)->company_id === Auth::user()->company_id) || Auth::user()->status == 'admin') {
            Order::active($request->order_id);
            Session::flash('message', "Naprawa ''$order->name'', została aktywowana na 7 dni");
            return redirect($request->back_to);
        } else {
            return redirect('error');
        }
    }

    public function addEmployee()
    {
        return view('boss.addEmployee');
    }

    public function addNewEmployee(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ], [], [
            'phone' => 'telefon',
            'email' => 'e-mail',
            'password' => 'Hasło'
        ]);

        User::createEmployee($request->name, $request->lastname, $request->password1, $request->email, $request->phone, Auth::user()->company_id);
        Session::flash('message', 'Dodano nowego pracownika');
        return redirect('/');

    }

    public function customers()
    {
        $customers = Customer::where('company_id', Auth::user()->company_id)->orderBy('lastname', 'asc')->paginate(10);
        return view('boss.customers')->with('customers', $customers);
    }

    public function customerOrders($id)
    {
        $customer = Customer::find($id);

        if ($customer == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || ($customer->company_id === Auth::user()->company_id)) {
            $orders = Order::where('customer_id', $id)->paginate(10);
            return view('boss.customerOrders')->with(['orders' => $orders, 'customer' => $customer]);
        } else {
            return redirect('error');
        }
    }

    public function deleteCustomer($id)
    {
        $customer = Customer::find($id);

        if ($customer == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || ($customer->company_id === Auth::user()->company_id)) {
            $ordersCount = Order::where('customer_id', $customer->customer_id)->count();
            return view('boss.deleteCustomer')->with(['customer' => $customer, 'ordersCount' => $ordersCount]);
        }
    }

    public function confirmDeletedCustomer(Request $request)
    {

        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        $customer = Customer::find($request->customer_id);

        if (($request == null) || ($customer == null)) {
            abort(404);
        }


        if ((Auth::user()->status === 'admin') || (($customer->company_id === Auth::user()->company_id) && Auth::user()->status == 'boss')) {
            if (!\Hash::check($request->password, \Auth::user()->password)) {
                $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
                return redirect()->route('editCustomer', ['id' => $request->customer_id])->withInput()->withErrors($errors);
            } else {
                Customer::find($request->customer_id)->delete();
                Session::flash('message', "Klient $customer->name $customer->lastname został usunięty wraz z przypisanymi do niego naprawami");
                return redirect($request->back_to);
            }
        }
    }

    public function employees($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || ((Auth::user()->company_id) === $company->company_id)) {
            $employees = User::where('company_id', $id)->where('status', 'employee')->orderBy('lastname', 'asc')->get();
            $bosses = User::where('company_id', $id)->where('status', 'boss')->where('user_id', '<>', Auth::user()->user_id)->get();
            return view('boss.employees')->with(['employees' => $employees, 'bosses' => $bosses]);
        } else {
            return redirect('error');
        }
    }

    public function deleteEmployee($id)
    {
        $employee = User::find($id);

        if ($employee == null) {
            abort(404);
        }

        $ordersCount = Order::where('employee_id', $employee->user_id)->count();
        $company = Company::find($employee->company_id);
        if ((Auth::user()->status === 'admin') || ((Auth::user()->company_id) === $employee->company_id)) {
            return view('boss.deleteEmployee')->with(['employee' => $employee, 'company' => $company, 'ordersCount' => $ordersCount]);
        } else {
            return redirect('error');
        }
    }

    public function confirmDeletedEmployee(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        $employee = User::find($request->employee_id);
        if ((Auth::user()->status === 'admin') || (Auth::user()->company_id === $employee->company_id)) {
            if (!\Hash::check($request->password, \Auth::user()->password)) {
                $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
                return redirect(url()->previous())->withInput()->withErrors($errors);
            } else {
                $employee->delete();
                Order::where('employee_id', $employee->user_id)->update(['employee_id' => null]);
                if (Auth::user()->status === 'admin') {
                    Session::flash('message', "Użytkownik ''$employee->name $employee->lastname'' został usunięty");
                } else {
                    Session::flash('message', "Pracownik ''$employee->name $employee->lastname'' został usunięty");
                }
                return redirect($request->back_to);
            }
        }
    }

    public function editEmployeeOrder($id)
    {
        if (Order::find($id) == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || (Auth::user()->company_id === Order::find($id)->company_id)) {
            $order = Order::find($id);
            $employees = User::where('company_id', $order->company_id)->where('status', 'employee')->where('user_id', '<>', $order->employee_id)->get();
            $employee = \App\User::find($order->employee_id);
            if ($order->employee_id === null) {
                return view('boss.editEmployeeOrder')->with(['order' => $order, 'employees' => $employees]);
            } else {
                $employeeOrder = User::find($order->employee_id);
                return view('boss.editEmployeeOrder')->with(['employee' => $employee, 'order' => $order, 'employeeOrder' => $employeeOrder, 'employees' => $employees]);
            }
        } else {
            return redirect('error');
        }
    }

    public function saveEditedEmployeeOrder(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        if (Order::find($request->order_id) == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || (Auth::user()->company_id === Order::find($request->order_id)->company_id)) {
            if (!\Hash::check($request->password, \Auth::user()->password)) {
                $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
                return redirect()->route('editEmployeeOrder', ['id' => $request->order_id])->withInput()->withErrors($errors);
            } else {
                Order::find($request->order_id)->update(['employee_id' => $request->employee]);
                Session::flash('message', "Zmieniono pracownika nadzorującego naprawę");
                return redirect($request->back_to);
            }
        } else {
            return redirect('error');
        }
    }

    public function deleteOrder($id)
    {
        $order = Order::find($id);

        if ($order == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || (Auth::user()->company_id === $order->company_id)) {
            Session::put('back_to', URL::previous());
            return view('boss.deleteOrder')->with(['order' => $order]);
        } else {
            return redirect('error');
        }
    }

    public function confirmDeletedOrder(Request $request)
    {
        $order = Order::find($request->order_id);

        if ($order == null) {
            abort(404);
        }

        if ((Auth::user()->status === 'admin') || (Auth::user()->company_id === $order->company_id)) {
            $order->delete();
            $back_to = Session::get('back_to');
            return redirect($back_to)->with('message', 'Zamówienie zostało usunięte');
        } else {
            return redirect('error');
        }
    }

    public function doBoss($id)
    {
        $user = User::find($id);

        if ($user == null) {
            abort(404);
        }

        if ((($user->company_id === Auth::user()->company_id) && $user->status === 'employee') || (Auth::user()->status == 'admin')) {
            return view('boss.doBoss')->with('user', $user);
        } else {
            return redirect('error');
        }
    }

    public function cofirmDoBoss(Request $request)
    {
        $user = User::find($request->user_id);

        if ($user == null) {
            abort(404);
        }

        if (($user->company_id === Auth::user()->company_id) && $user->status === 'employee') {
            $user->update(['status' => 'boss']);
            $user->save();

            return redirect($request->back_to);
        } else {
            return redirect('error');
        }

    }

    public function doEmployee($id)
    {
        $user = User::find($id);

        if ($user == null) {
            abort(404);
        }

        if ((($user->company_id === Auth::user()->company_id) && $user->status === 'boss') || (Auth::user()->status == 'admin')) {
            return view('boss.doEmployee')->with('user', $user);
        } else {
            return redirect('error');
        }
    }

    public function cofirmDoEmployee(Request $request)
    {
        $user = User::find($request->user_id);

        if ($user == null) {
            abort(404);
        }

        if (($user->company_id === Auth::user()->company_id) && $user->status === 'boss') {
            $user->update(['status' => 'employee']);
            $user->save();

            return redirect($request->back_to);
        } else {
            return redirect('error');
        }

    }

}