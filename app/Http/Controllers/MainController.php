<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Company;
use App\Customer;
use App\Http\Requests\CheckOrderRequest;
use App\Mail\SendMailToAuthor;
use App\User;
use App\Order;
use App\Status;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use App\Mail\QuestionToOrder;
use function MongoDB\BSON\toJSON;
use function Symfony\Component\VarDumper\Tests\Fixtures\bar;


class MainController extends Controller
{
    public function contact()
    {
        return \view('contact');
    }

    public function forCompany()
    {
        return \view('forCompany');
    }

    public function home()
    {
        if (Auth::guest()) {
            return view('index');
        } elseif (Auth::user()->status === 'boss') {
            $users = \App\User::where('company_id', Auth::user()->company_id)->where('status', 'employee')->get();
            $orders = \App\Order::where('company_id', Auth::user()->company_id)->where('active', 'true')->orderBy('updated_at', 'desc')->take(10)->get();
            $activeOrdersCount = Order::where('company_id', Auth::user()->company_id)->where('active', 'true')->count();
            return view('boss')->with(['users' => $users, 'orders' => $orders, 'activeOrdersCount' => $activeOrdersCount]);

        } elseif (Auth::user()->status === 'employee') {
            $user_id = Auth::user()->user_id;
            $orders = Order::where('employee_id', $user_id)->where('active', 'true')->orderBy('updated_at', 'desc')->get();
            return view('employee')->with('orders', $orders);

        } elseif (Auth::user()->status === 'admin') {
            $companies = Company::orderBy('created_at', 'desc')->take(10)->get();
            $users = User::where('status', '<>', 'admin')->orderBy('created_at', 'desc')->take(10)->get();
            $orders = Order::orderBy('created_at', 'desc')->take(10)->get();
            $customers = Customer::orderBy('created_at', 'desc')->take(10)->get();
            return view('admin')->with(['companies' => $companies, 'users' => $users, 'orders' => $orders, 'customers' => $customers]);
        }
    }


    public function show($id)
    {
        $order = Order::findOrFail($id);
        $name = $order->name;
        return view('test2')->with('id', $id);
    }

    public function check(CheckOrderRequest $request)
    {
        $token = $request->input('token');
        $lastname = $request->input('lastname');
        Session::put(['token' => $token, 'lastname' => $lastname]);

        if (Order::where('token', $token)->get()->count() == 0) {
            Session::flash('message', 'Nie ma takiej naprawy w bazie. Sprawdź dane i spróbuj ponownie');
            return \redirect('/')->withInput();
        } elseif (Order::where('token', $token)->first()->customer->lastname <> $lastname) {
            Session::flash('message', 'Wprowadzone dane nie zgadzają się. Sprawdź dane i spróbuj ponownie');
            return \redirect('/')->withInput();
        } elseif ((Order::where('token', $token)->first()->customer->lastname === $lastname) && (Order::where('token', $token)->first()->active === 'false')) {
            Session::flash('message', 'Naprawa nie jest już aktywna');
            return \redirect('/')->withInput();
        } else {
            $id = Order::where('token', $token)->first()->order_id;
            $order = Order::find($id);
            $bills = Bill::where('order_id', $id)->get();
            $sum = Bill::where('order_id', $order->order_id)->sum('cost');
            $posts = Post::where('order_id', $id)->orderBy('created_at', 'desc')->get();
            return view('check')->with(['order' => $order, 'bills' => $bills, 'sum' => $sum, 'posts' => $posts]);
        }
    }

    public function sendMail()
    {
        $user = User::find(11);
        $subject = 'Tytuł';
        $content = [
            'subject' => $subject,
            'user' => $user,
        ];
        Mail::to($user)->send(new QuestionToOrder($content));

        return ("Poszło!");
    }

    public function sendQuestionToOrder(Request $request)
    {
        $order = Order::find($request->order_id);
        $customer = Customer::find($order->customer_id);
        $question = $request->question;
        $subject = "Pytanie o naprawę " . $order->name;
        $bosses = User::where('company_id', $order->company_id)->where('status', 'boss')->get();
        $infoBoss = null;

        if ($order->employee_id <> null) {
            $employee = User::find($order->employee_id);
            $infoBoss = "Naprawę nadzoruje $employee->name $employee->lastname";
        }

        if ($order->employee_id <> null) {
            $content = [
                'user' => $employee,
                'customer' => $customer,
                'order' => $order,
                'question' => $question,
                'subject' => $subject
            ];
            Mail::to($employee)->send(new QuestionToOrder($content));
        }


        foreach ($bosses as $boss) {

            $contentBoss = [
                'user' => $boss,
                'customer' => $customer,
                'order' => $order,
                'question' => $question,
                'subject' => $subject,
                'infoBoss' => $infoBoss,

            ];
            Mail::to($boss)->send(new QuestionToOrder($contentBoss));
        }

        return \redirect()->route('check', ['lastname' => $customer->lastname, 'order_id' => $order->token])->with('message', 'Pytanie zostało wysłane');
    }

    public function sendMailToAuthor(Request $request)
    {
        $admins = User::where('status', 'admin')->get();
//        return $admins;
        foreach ($admins as $admin) {
            $content = [
                'admin' => $admin,
                'text' => $request->text,
                'email' => $request->email
            ];

            Mail::to($admin)->send(new SendMailToAuthor($content));
        }
        return back()->with('message', 'Wiadomość została wysłana');

    }


    public function error()
    {

        return view('error');
    }


}
