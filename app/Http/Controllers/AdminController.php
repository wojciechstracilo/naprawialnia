<?php

namespace App\Http\Controllers;

use App\Company;
use App\Customer;
use App\Mail\NewOrder;
use App\Order;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;

class AdminController extends Controller
{

    public function company($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        $bosses = User::where('company_id', $id)->where('status', 'boss')->get();
        $employees = User::where('company_id', $id)->where('status', 'employee')->orderBy('lastname', 'asc')->paginate(10);
        $customersCount = Customer::where('company_id', $id)->count();
        $ordersActive = Order::where('company_id', $id)->where('active', 'true')->count();
        $ordersArchive = Order::where('company_id', $id)->where('active', 'false')->count();
        return view('admin.company')->with(['company' => $company, 'bosses' => $bosses, 'employees' => $employees, 'customersCount' => $customersCount,
            'ordersActive' => $ordersActive, 'ordersArchive' => $ordersArchive]);
    }

    public function editCompany($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        return view('admin.editCompany')->with(['company' => $company]);
    }

    public function editAccount($id)
    {
        $user = User::find($id);

        if ($user== null) {
            abort(404);
        }

        return view('admin.editAccountt')->with('user', $user);
    }

    public function saveEditedAccount(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|confirmed'
        ], [], [
            'password' => 'Hasło'
        ]);

        if (!\Hash::check($request->password, \Auth::user()->password)) {
            $errors = new MessageBag(['password' => 'Hasło nie jest poprawne']);
            return redirect()->route('editAccount', ['id' => $request->user_id])->withInput()->withErrors($errors);
        } else {
            User::updateEmployee($request->user_id, $request->name, $request->lastname, $request->email, $request->phone);
            \Session::flash('message', 'Zaktualizowano dane profilowe użytkownika');
            return redirect($request->back_to);
        }
    }

    public function companyOrders($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        $orders = Order::where('company_id', $id)->where('active', 'true')->orderBy('name', 'asc')->paginate(10);
        return view('admin.companyOrders')->with(['orders' => $orders, 'company' => $company]);
    }

    public function companyArchiveOrders($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        $orders = Order::where('company_id', $id)->where('active', 'false')->orderBy('updated_at', 'desc')->paginate(10);
        return view('admin.companyArchiveOrders')->with(['orders' => $orders, 'company' => $company]);
    }

    public function companyCustomers($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        $customers = Customer::where('company_id', $company->company_id)->paginate(10);
        return view('admin.companyCustomers')->with(['company' => $company, 'customers' => $customers]);
    }

    public function companies()
    {
        $comapnies = Company::orderBy('name', 'asc')->paginate(10);
        return view('admin.companies')->with(['companies' => $comapnies]);
    }

    public function deleteCompany($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        $boss = User::where('company_id', $company->company_id)->where('status', 'boss')->first();
        Session::put('back_to', \URL::previous());
        return view('admin.deleteCompany')->with(['company' => $company, 'boss' => $boss]);
    }

    public function confirmDeletedCompany(Request $request)
    {
        $company = Company::find($request->company_id);

        if ($company == null) {
            abort(404);
        }

        $name = $company->name;
        $company->delete();
        $back_to = Session::get('back_to');
        return redirect($back_to)->with('message', "Firma ''$name'' została usunięta");
    }

    public function addCompany()
    {
        return view('admin.addCompany');
    }

    public function createCompany(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'companyName' => 'required|unique:companies,name',
            'companyPhone' => 'required|unique:companies,phone',
            'description' => 'required'
        ], [
            'companyName.unique' => 'Ta nazwa firmy jest zajęta',
            'companyPhone.unique' => 'Ten numer telefonu jest zajęty',
        ]);


        $company = Company::create([
            'name' => $request['companyName'],
            'phone' => $request['companyPhone'],
            'address' => $request['companyAddress'],
            'description' => $request['description']
        ]);

        User::create([
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'company_id' => $company->company_id,
            'password' => bcrypt($request['password']),
        ]);
        Session::flash('message', "Firma ''$company->name'' została utworzona");

        return redirect('/companies');
    }

    public function addEmployee($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        Session::put('back_to', \URL::previous());
        return view('admin.addEmployee')->with(['company' => $company]);
    }

    public function addEmployeeToCompany(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed'
        ], [], [
            'phone' => 'telefon',
            'email' => 'e-mail',
            'password' => 'Hasło'
        ]);

        User::createEmployee($request->name, $request->lastname, $request->password, $request->email, $request->phone, $request->company_id);
        Session::flash('message', "Dodano nowego pracownika do firmy");
        $back_to = Session::get('back_to');
        return redirect($back_to);

    }

    public function allCustomers()
    {
        $customers = Customer::orderBy('lastname', 'acs')->paginate(10);
        return view('admin.allCustomers')->with('customers', $customers);
    }

    public function allUsers()
    {
        $users = User::where('user_id', '<>', \Auth::user()->user_id)->orderBy('lastname', 'asc')->paginate(10);
        return view('admin.allUsers')->with('users', $users);
    }

    public function allOrders()
    {
        $orders = Order::where('active', 'true')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.allOrders')->with('orders', $orders);
    }

    public function allArchiveOrders()
    {
        $orders = Order::where('active', 'false')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.allArchiveOrders')->with('orders', $orders);
    }

    public function addOrderToCompany($id)
    {
        $company = Company::find($id);

        if ($company == null) {
            abort(404);
        }

        $customers = \App\Customer::where('company_id', $company->company_id)->orderBy('lastname', 'asc')->get();
        $employees = \App\User::where('company_id', $company->company_id)->where('status', 'employee')->get();
        return view('admin.addOrderToCompany')->with(['company' => $company, 'customers' => $customers, 'employees' => $employees]);
    }

    public function addNewOrderToCompany(Request $request)
    {

        if ($request == null) {
            abort(404);
        }

        if ($request->radio === 'new') {
            $newCustomerId = Customer::createCustomer($request->customerName, $request->customerLastName, $request->address, $request->email, $request->phone, $request->company_id);
            $newOrderId = Order::createOrder($request->name, $request->employee, $newCustomerId, $request->company_id);
            Post::createPost('Sprzęt przyjęty do naprawy', $newOrderId);

            $order = Order::find($newOrderId);
            $customer = Customer::find($newCustomerId);
            $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;
            $subject = "Naprawa ''$order->name'' rozpoczęła się";
            $company = Company::find($order->company_id);
            $content = [
                'order' => $order,
                'customer' => $customer,
                'redirect' => $redirect,
                'subject' => $subject,
                'company' => $company
            ];
            \Mail::to($customer)->send(new NewOrder($content));

            Session::flash('message', 'Dodano nową naprawę');
            return redirect()->route('company', ['id' => $request->company_id]);

        } elseif ($request->radio === 'old') {
            $newOrderId = Order::createOrder($request->name, $request->employee, $request->oldCustomer, $request->company_id);
            Post::createPost('Sprzęt przyjęty do naprawy', $newOrderId);

            $order = Order::find($newOrderId);
            $customer = Customer::find($request->oldCustomer);
            $redirect = 'http://127.0.0.1:8000/check?lastname=' . $customer->lastname . '&token=' . $order->token;
            $subject = "Naprawa ''$order->name'' rozpoczęła się";
            $company = Company::find($order->company_id);
            $content = [
                'order' => $order,
                'customer' => $customer,
                'redirect' => $redirect,
                'subject' => $subject,
                'company' => $company
            ];
            \Mail::to($customer)->send(new NewOrder($content));

            Session::flash('message', 'Dodano nową naprawę');
            return redirect()->route('company', ['id' => $request->company_id]);

        } else {
            return redirect('error');
        }

    }


}
