<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\MessageBag;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    protected $message = 'Firma została utworzona. Możesz zalogować się na swoje konto';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|string|max:30',
            'lastname' => 'required|string|max:30',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|integer|unique:users,phone',
            'email' => 'required|email|max:50|unique:users,email',
            'companyName' => 'required|max:50|unique:companies,name',
            'companyPhone' => 'required|integer|unique:companies,phone',
            'description' => 'required|max:200',
        ],[
            'companyName.unique' => 'Ta nazwa firmy jest zajęta',
            'companyPhone.unique' => 'Ten numer telefonu jest zajęty',
            'phone.integer' => 'Telefon musi zawierać same cyfry',
            'companyPhone.integer' => 'Telefon musi zawierać same cyfry',
        ],[
            'email' => 'e-mail',
            'phone' => 'telefon'
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $company = Company::create([
            'name' => $data['companyName'],
            'phone' => $data['companyPhone'],
            'address' => $data['companyAddress'],
            'description' => $data['description'],
        ]);

        Session::flash('message', 'Firma została utworzona. Możesz zalogować się na swoje konto');

        return User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'company_id' => $company->company_id,
            'password' => bcrypt($data['password']),
        ]);

    }
}
