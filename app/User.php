<?php

namespace App;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $primaryKey = 'user_id';

    public $incrementing = false;

    use Notifiable, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'lastname',  'password', 'status', 'email', 'phone', 'company_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Funkcja pobiera identyfikator (ID) użytkownika i zwraca wszystkie jego dane
     * @param $user_id - Identyfikator (ID) użytkownika
     * @return zwraca wszystkie dane użytkownika
     */
    public static function getUser($user_id){
        $user = User::find($user_id);
        return $user;
    }

    public static function createEmployee($name, $lastname, $password, $email, $phone, $company_id){
        $pass = bcrypt($password);
        $employee = new User(['name' => $name, 'lastname' => $lastname, 'password' => $pass, 'email' => $email, 'phone' => $phone, 'company_id' => $company_id, 'status' => 'employee']);
        $employee->save();
    }

    public static function updateEmployee($employee_id, $name, $lastname, $email, $phone){
        $employee = User::find($employee_id);
        $employee->name = $name;
        $employee->lastname = $lastname;
        $employee->email = $email;
        $employee->phone = $phone;
        $employee->save();
    }

    public static function updateAccount($user_id, $name, $lastname, $email, $phone){
        $user = User::find($user_id);
        $user->name = $name;
        $user->lastname = $lastname;
        $user->email = $email;
        $user->phone = $phone;
        $user->save();
    }

    public static function newPassword($user_id, $newPassword){
        $user = User::find($user_id);
        $user->password = Hash::make($newPassword);
        $user->save();
    }

    /**
     * Uzytkownik moze pracowac tylko dla jednej firmy
     */
    public function company(){
        return $this->belongsTo('App\Company');
    }

    /**
     * Uzytkownik moze posiadac wiele zlecen
     */
    public function orders(){
        return $this->hasMany('App\Order', 'employee_id');
    }

    /**
     * Uzytkownik moze dodawac wiele postow
     */
    public function posts(){
        return $this->hasMany('App\Post');
    }
}
