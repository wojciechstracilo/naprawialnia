<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $primaryKey = 'company_id';

    protected $fillable  = [
        'company_id', 'name', 'address', 'phone', 'description'
    ];

    public static function updateCompany($company_id, $name, $address, $phone, $description){
        $company = Company::find($company_id);
        $company->name = $name;
        $company->address = $address;
        $company->phone = $phone;
        $company->description = $description;
        $company->save();

        return $company;
    }

    /**
     * Firma moze miec wielu uzytkownikow
     */
    public function users(){
        return $this->hasMany('App\User', 'company_id');
    }
}
