<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $primaryKey = 'status_id';

    protected $fillable  = [
        'status_id', 'name', 'company_id'
    ];

    public static function getIdByName($name){
        $status_id = Status::where('name', $name)->get()->first()->status_id;
        return $status_id;
    }

    /**
     * Status może być używany przez wiele zamowien
     */
    public function order(){
        return $this->belongsTo('App\Order');
    }
}
