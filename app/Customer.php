<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Post
{
    protected $primaryKey = 'customer_id';

    protected $fillable  = [
        'customer_id', 'name', 'lastname', 'address', 'email', 'phone', 'company_id'
    ];

    /**
     * Klient moze posiadac wiele zamowien
     */
    public function orders(){
        return $this->hasMany('App\Order', 'order_id');
    }

    /**
     * Funkcja tworzy nowego klienta z podanych danych i zwraca jego identyfikator (ID)
     * @param $name - Imie klienta
     * @param $lastname - Nazwisko klienta
     * @param $address - Adres klienta
     * @param $email - Mail klienta
     * @param $phone - Telefon klienta
     * @return customer_id - Funkcja zwraca ID utworzonego klienta
     */
    public static function createCustomer($name, $lastname, $address, $email, $phone, $company_id){
        $newCustomer = new Customer(['name' => $name, 'lastname' => $lastname, 'address' => $address, 'email' => $email, 'phone' => $phone, 'company_id' => $company_id]);
        $newCustomer ->save();

        return $newCustomer->customer_id;
    }

    public static function getCustomer($id){
        $customer = Customer::find($id);
        return $customer;
    }

    public static function updateCustomer($customer_id, $name, $lastname, $address, $email, $phone){
        $customer = Customer::find($customer_id);
        $customer->update([ 'name' => $name, 'lastname'=> $lastname, 'address' => $address, 'email' => $email, 'phone' => $phone ]);
        $customer->save();

        return $customer->customer_id;
    }
}
