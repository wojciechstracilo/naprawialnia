<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $primaryKey = 'order_id';

    protected $fillable  = [
        'order_id', 'token', 'name', 'status_id', 'employee_id', 'active', 'customer_id', 'company_id'
    ];

    /**
     * Funkcja tworzy nowe zamowienie z podanych danych i zwraca jego identyfikator (ID)
     * @param $name - Nazwa zamowienia
     * @param $employee_id - Login pracownika nadzorujacego zamowienie
     * @param $customer_id - Identyfikator (ID) klienta, ktorego dotyczy zamowienie
     * @param $company_id - Identyfikator (ID) firmy, w ktorej zlozono zamowienie
     * @return order_id - Funkcja zwraca identyfikator (ID) utworzonego zamowienia
     */
    public static function createOrder($name, $employee_id, $customer_id, $company_id){
        $token = str_random(10);

        $isExist = Order::where('token', $token)->get();
        while (!$isExist->isEmpty())
        {
            $token= str_random(10);
            $isExist = Order::where('token', $token)->get();
        }

        $newOrder = new Order(['token' => $token, 'name' => $name, 'employee_id' => $employee_id, 'customer_id' => $customer_id, 'company_id' => $company_id, 'status_id' => 1, 'active' => 'true']);
        $newOrder ->save();

        return $newOrder->order_id;
    }

    public static function updateNameOrder($order, $newName){
        $order->update(['name' => $newName]);
        $order->save();

        return $order->order_id;
    }

    public static function archive($id){
        $order = Order::find($id);
        $order->active = 'false';
        $order->save();
        $post = new Post(['order_id' => $order->order_id, 'text' => 'Zamówienie przeniesiono do archiwum']);
        $post->save();
    }

    public static function active($id){
        $order = Order::find($id);
        $order->active = 'true';
        $order->save();
        $post = new Post(['order_id' => $order->order_id, 'text' => 'Zamówienie zostało aktywowane']);
        $post->save();
    }

    public static function updateStatus($order_id, $status_id){
        $order = Order::find($order_id);
        $order->status_id = $status_id;
        $order->save();
    }

    /**
     * Kazde zamowienie moze miec wiele rachunkow do zaplaty
     */
    public function bills(){
        return $this->hasMany('App\Bill');
    }

    /**
     * Kazde zamowienie jest przypisane do jednego pracownika
     */
    public function user(){
        return $this->belongsTo('App\User', 'employee_id');
    }

    /**
     * Kazde zamowienie jest przypisane do jednego klienta
     */
    public function customer(){
        return $this->belongsTo('App\Customer', 'customer_id');
    }

    /**
     * Kazde zamowienie ma przypisany status
     */
    public function status(){
        return $this->belongsTo('App\Status', 'status_id');
    }

    /**
     * Kazde zamowienie jest przypisane do firmy
     */
    public function company(){
        return $this->belongsTo('App\Company', 'company_id');
    }
}
